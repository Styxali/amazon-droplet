const cron = require('node-cron');
const express = require('express');
const checkAmazon = require('./amazon.js')


app = express();

// Schedule tasks to be run on the server.
cron.schedule('20 * * * * *', function() {
  console.log('running a task every minute');
});

app.listen(3000);