var cheerio = require('cheerio')


const getAllData = (doc) => {
    let $ = cheerio.load(doc)
    {
        let arr1 = $('.pdTab table tbody tr').toArray().map(r => {
            if (!$(r).find('td.label'))
                return;
            let key = ($(r).find('td.label') ? $(r).find('td.label').text().trim() : "")
            let value = ($(r).find('td.value') ? $(r).find('td.value').text().trim() : "")
            return {
                key: key,
                value: value

            }
        }
        ).filter(r => !!r);
        let arr2 = $('#HLCXComparisonTable tbody tr').toArray().map(r => {
            if (!$(r).find('th'))
                return;
            let key = ($(r).find('th') ? $(r).find('th').text().trim() : "")
            let value = ($(r).find('td.comparison_baseitem_column') ? $(r).find('td.comparison_baseitem_column').text().trim() : "")
            // 	r.text().trim()
            return {
                key: key,
                value: value

            }

        }
        ).filter(r => !!r)
        let arr3 = $('td.bucket .content ul li').toArray().map(r => {
            if (!$(r).find('b'))
                return;
            let key = ($(r).find('b') ? $(r).find('b').text().trim().split(':')[0].trim() : "")
            let value = ($(r).text().trim() ? $(r).text().trim().split(key)[1].trim() : "")
            // 	r.text().trim()
            return {
                key: key,
                value: value

            }

        }
        ).filter(r => !!r)
        let arr4 = $('table[id^="tech-specs-table"] tbody tr').toArray().map(r => {
            if (!$(r).find('td'))
                return;
            let key = ($(r).find('td') ? $(r).find('td.a-text-bold').text().trim() : "")
            let value = ($(r).find('td') ? $(r).find('td:nth-child(2)').text().trim() : "")
            // 	r.text().trim()
            return {
                key: key,
                value: value

            }

        }
        ).filter(r => !!r)
        let arr5 = $('table.ucc-comparison-table tbody tr').toArray().map(r => {
            if (!$(r).find('td.ucc-attribute-title'))
                return;
            let key = ($(r).find('td.ucc-attribute-title') ? $(r).find('td.ucc-attribute-title').text().trim() : "")
            let value = ($(r).find('td:nth-child(2)') ? $(r).find('td:nth-child(2)').text().trim() : "")
            // 	r.text().trim()
            return {
                key: key,
                value: value

            }

        }
        ).filter(r => !!r)
        let arr6 = $('table[id^="productDetails_"] tbody tr').toArray().map(r => {
            if (!$(r).find('th'))
                return;
            let key = ($(r).find('th') ? $(r).find('th.prodDetSectionEntry').text().trim() : "")
            let value = ($(r).find('td') ? $(r).find('td').text().trim() : "")
            // 	r.text().trim()
            return {
                key: key,
                value: value

            }

        }
        ).filter(r => !!r)
        let arr7 = $('ul li .a-list-item').toArray().map(r => {
            if (!$(r).find('span.a-text-bold'))
                return;
            let key = ($(r).find('span.a-text-bold') ? $(r).find('span.a-text-bold').text().trim() : "")
            let value = ($(r).find('span:nth-child(2)') ? $(r).find('span:nth-child(2)').text().trim() : "")
            // 	r.text().trim()
            return {
                key: key,
                value: value

            }

        }
        ).filter(r => !!r)
        let arr8 = $('table[id^="technicalSpecifications_"] tbody tr').toArray().map(r => {
            if (!$(r).find('th')) return;
            let key = ($(r).find('th') ? $(r).find('th').text().trim() : "")
            let value = ($(r).find('td') ? $(r).find('td').text().trim() : "")
            // 	r.text().trim()
            return {
                key: key,
                value: value

            }

        }).filter(r => !!r)
        let metadata = [...arr1, ...arr2, ...arr3, ...arr4, ...arr5, ...arr6, ...arr7, ...arr8]
        return metadata.filter(v => (v["key"].trim() !== "" && v["key"].trim() !== ""))
    }

}
const toFloat = (str) => {

    let pattern = new RegExp("([\\d\\.]+)")
    // console.log(str)
    str = str.replace(',', '.')
    let price = parseFloat(str.match(pattern)[1]).toFixed(2) * 100
    return Math.round(price) / 100
}
module.exports.getAllData = getAllData;
module.exports.getDimensions = (doc) => {
    let info = {};
    let $ = cheerio.load(doc)
    if ($('.prodDetSectionEntry').length) {
        $('.prodDetSectionEntry').toArray().forEach(r => {
            //         console.log(r.text().trim())

            switch ($(r).text().trim()) {
                case 'Package Weight':
                    if (info.hasOwnProperty('shippingWeight'))
                        break;
                    info.shippingWeight = $(r).parent().find('td').text().trim().split('(')[0].trim()
                    break;
                case 'Shipping Weight':
                    info.shippingWeight = $(r).parent().find('td').text().trim().split('(')[0].trim()
                    break;
                case 'Item Weight':
                    if (info.hasOwnProperty('shippingWeight'))
                        break;
                    info.shippingWeight = $(r).parent().find('td').text().trim().split('(')[0].trim()
                    break;
                case 'Item Dimensions L x W x H':
                    if (info.hasOwnProperty('productDimensions'))
                        break;
                    info.productDimensions = $(r).parent().find('td').text().trim()
                    break;
                case 'Product Dimensions':
                    info.productDimensions = $(r).parent().find('td').text().trim()
                    break;
                default:
                    break;
            }

        }
        )
    } else if (!!$('tech-specs-table-right')) {
        $('#tech-specs-table-right tbody tr td p').toArray().forEach(el => {
            if ($(el).text().trim().indexOf('Height') != -1) info.height = $(el).text().trim()
            if ($(el).text().trim().indexOf('Weight') != -1) info.weight = $(el).text().trim()
            if ($(el).text().trim().indexOf('Width') != -1) info.width = $(el).text().trim()
            if ($(el).text().trim().indexOf('Depth') != -1) info.length = $(el).text().trim()

        })
    }
    let measures = {}
    measures.volumicWeight = 500
    if (info.hasOwnProperty('shippingWeight')) {
        //              console.log(info.shippingWeight)
        let coef = 1
        if (info["shippingWeight"].indexOf('ounces') != -1) {
            coef = 28.35
            let weight = info["shippingWeight"].split('ounces')[0].trim()
            measures.weight = Number(weight) * coef
        }
        if (info["shippingWeight"].indexOf('pounds') != -1) {
            coef = 453.6
            let weight = info["shippingWeight"].split('pounds')[0].trim()
            measures.weight = Number(weight) * coef
        }
        measures.ogWeight = info["shippingWeight"]
        measures.volumicWeight = (measures.weight > 500 ? measures.weight : measures.volumicWeight)
    }


    if (info.hasOwnProperty('productDimensions')) {
        // console.log(info["productDimensions"])
        let coef = 1
        if (info["productDimensions"].indexOf('inches') != -1) {
            coef = 2.54
            let dims = info["productDimensions"].split('inches')[0].trim()
            dims.split('x').forEach((v, i, arr) => {

                measures.length = Number(arr[0]) * coef
                measures.width = Number(arr[1]) * coef
                measures.height = Number(arr[2]) * coef


            })
        }
        if (info["productDimensions"].indexOf(';') != -1) {

            let weightDim = info["productDimensions"].split(';')[1].trim()
            if (weightDim.indexOf('pounds') != -1) {
                let coef2 = 453.6
                let wR = weightDim.split('pounds')[0].trim()
                if (typeof measures.weight === "undefined") measures.weight = Number(wR) * coef2
                //    console.log(measures.weight)

                measures.volumicWeight = (measures.weight > 500 ? measures.weight : measures.volumicWeight)

            }
        }
        measures.size = info["productDimensions"]
        let volW = measures.length * measures.height * measures.width / 5

        if (volW > 500) measures.volumicWeight = volW

    }

    // console.log(measures)
    return measures
}
module.exports.getCategories = (doc) => {

}
module.exports.getFlashTime = (doc) => {
    let $ = cheerio.load(doc)
    try {

        console.log("flashTime")
        if ($('[id^=deal_expiry_timer]').length) {
            if ($('[id^=deal_expiry_timer]').text().trim().indexOf('dans') != -1) {
                return $('[id^=deal_expiry_timer]').text().trim().split('dans ')[1]
            }

        } else {
            return $('[id^=deal_expiry_timer]').text().trim().split('in ')[1]

        }
        return "0"
    } catch (error) {
        console.log(error)
        return "0"

    }
}
module.exports.getFlashPrice = (doc) => {
    let $ = cheerio.load(doc)
    return $("#priceblock_dealprice").text().trim()

}
module.exports.hasForestTaxe = (doc) => {
    let $ = cheerio.load(doc)
    let taxe = false
    if ($('#feature-bullets').text().trim().indexOf('PEFC') != -1 || $(doc).find('#feature-bullets').text().trim().indexOf('FSC100') != -1 || $(doc).find('#productDescription').text().trim().indexOf('FSC 100') != -1) taxe = (taxe == true ? taxe : true)
    if ($('#productDescription').text().trim().indexOf('PEFC') != -1 || $(doc).find('#productDescription').text().trim().indexOf('FSC100') != -1 || $(doc).find('#productDescription').text().trim().indexOf('FSC 100') != -1) taxe = (taxe == true ? taxe : true)
    if ($('#HLCXComparisonTable').length) {
        // console.log("heeey")
        $("#HLCXComparisonTable tbody tr").each((idx, li) => {
            // console.log(($(li).find("td.comparison_baseitem_column").text().trim(),$(li).find("td.comparison_baseitem_column").text().trim().toLowerCase().indexOf("bois")!=-1))
            if ($(li).text().indexOf('Matériau') != -1) taxe = (taxe == true ? taxe : ($(li).find("td.comparison_baseitem_column").text().trim().toLowerCase().indexOf("bois") != -1))
        })
    }
    if ($("#prodDetails").length) {
        // console.log("Cas 3 weight")
        $(" tbody tr").each((idx, li) => {
            // console.log($(li).find('td:nth-child(2)').text().trim(),$(li).find('td:nth-child(2)').text().trim().toLowerCase().indexOf("bois")!=-1)
            contentLi = $(li).find('td:nth-child(1)').text().trim().toLowerCase()
            if (contentLi.indexOf('matériau') !== -1 || contentLi.indexOf('matière principale') !== -1 || contentLi.indexOf('composition') !== -1) taxe = (taxe == true ? taxe : ($(li).find('td:nth-child(2)').text().trim().toLowerCase().indexOf("bois") != -1))
        })


    }
    // console.log("extra",taxe)
    return taxe
}
module.exports.hasFlash = (doc) => {
    let $ = cheerio.load(doc)
    if ($('[id^=deal_expiry_timer]').length || $("span.gb-accordion-active", "#LDBuybox").text().trim().indexOf("Vente Flash") != -1) return true
    return false
}
module.exports.hasVat = (doc) => { let $ = cheerio.load(doc) }
module.exports.isFreeShippingOk = (doc) => {
    let $ = cheerio.load(doc)
    return $("#price-shipping-message").text().trim().indexOf("EUR 25") != -1
}

module.exports.isPanierPlus = (doc) => {
    let $ = cheerio.load(doc)
    if ($('.a-box-group')) return $('.a-box-group').text().trim().indexOf('Article Panier Plus') != -1

}
module.exports.isPrime = (doc) => {
    let $ = cheerio.load(doc)
    return $("#bbop_feature_div").text().indexOf("Acquisition_AddToCart_PrimeBasicFreeTrialUpsellEligible") != -1 || $("#merchant-info").text().indexOf("Amazon") != -1

    // return $("#bbop_feature_div").text().trim().indexOf("Acquisition_AddToCart_PrimeBasicFreeTrialUpsellEligible") != -1
}
module.exports.isSoldByAmazon = (doc) => {
    let $ = cheerio.load(doc)
    if ($('#bylineInfo')) return $('#bylineInfo').text().trim().indexOf('Amazon') != -1
    return false
}

module.exports.getSellerId = (doc) => { let $ = cheerio.load(doc) }
module.exports.getSellerLink = (doc) => {
    let $ = cheerio.load(doc);
    return ($('#merchant-info a').toArray()[0] ? $('#merchant-info a').toArray()[0].href : "")
}
module.exports.isBook = (categ, doc) => {
    let $ = cheerio.load(doc)
    try {

        isBookOk = false;
        if (isBookOk == false && doc && !!$('.prodDetSectionEntry').length) {
            let entries = $('.prodDetSectionEntry').toArray().map((r) => $(r).text());
            isBookOk = entries.some((it) => {
                if (it.toLowerCase().trim().indexOf('isbn') != -1)
                    return true;
                if (it.toLowerCase().trim().indexOf('livre') != -1)
                    return true;

            }
            );
        }
        return isBookOk || categ.some(el => el.split('>')[0].toLowerCase().indexOf("livres") != -1)
    } catch (error) {
        console.log(error)
        return false
    }

}
module.exports.getSize = (doc) => {
    let $ = cheerio.load(doc);
    let dataSize = getAllData(doc).filter(met => {
        return (met["key"].toLowerCase().indexOf("size") != -1 || met["key"].toLowerCase().indexOf("dimensions du colis") != -1 || met["key"].indexOf('Dimensions du produit') != -1)
    })
    if (dataSize.length != -1) {
        if (dataSize[0] && (dataSize[0]["value"].indexOf("—") != -1 || dataSize[0]["value"].indexOf("<") != -1)) {
            return ""
        }
        if (dataSize[0]) {
            return dataSize[0]["value"]
        }
    }
    return ""
}
module.exports.getWeight = (doc) => {
    let $ = cheerio.load(doc);
    let dataWeight = getAllData(doc).filter(met => {
        return (met["key"].toLowerCase().indexOf("poids") != -1 || met["key"].toLowerCase().indexOf("weight") != -1 || met["key"].indexOf("Poids de l'article") != -1)
    });
    if (dataWeight.length != -1) {
        if (dataWeight[0] && (dataWeight[0]["value"].indexOf("—") != -1 || dataWeight[0]["value"].indexOf("<") != -1)) {
            return "";
        }
        if (dataWeight[0]) {
            return dataWeight[0]["value"]
        }
    }
    return ""
}

module.exports.getDeliveryDate = (doc) => { let $ = cheerio.load(doc) }
module.exports.getDeliveruDuration = (doc) => { let $ = cheerio.load(doc) }
