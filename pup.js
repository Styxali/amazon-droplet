const fs = require("fs");
const puppeteer = require("puppeteer"); //puppeteer, a headless chromium browser
const cart = require('./cart.json');
const iPhone = puppeteer.devices['iPhone 6'];
let device = false;
/*
Note: Puppeteer, being a headless browser, is pretty slow due to the usual overheads of a browser, 
    and I did not realise that until too late into the assignment. Probably will switch to scrapy
  & BeautifulSoup with python next time
*/

(async () => {
    try {

        console.log('opening');
        // open browser
        const browser = await puppeteer.launch({args: ['--no-sandbox'], headless: true, devtools: false, userDataDir: '/tmp/myChromeSession' });
        console.log('opened');
        //read the contents of the CSV file and split them on new line
        let asins = fs.readFileSync("./asins.csv", { encoding: "utf-8" }).split("\r\n");
        //remove title(first row)
        asins = asins.slice(1, asins.length);

        //create the output CSV File
        fs.writeFileSync(
            "./output.csv",
            "ASIN,Name,Price,Category,Sub-Category,Images,Description,Rating,Number of Ratings,Best Seller Rank,Brand,Manufacturer,Weight,Dimensions\n"
        );

        //open a new page on the browser
        const page = await browser.newPage();
let device = false;
if(process.argv[2] =="--device")await page.emulate(iPhone);
        
        // await page.setRequestInterception(true);
        // page.on('request', (req) => {
        //     if (req.resourceType() == 'stylesheet' || req.resourceType() == 'font' || req.resourceType() == 'image') {
        //         req.abort();
        //     }
        //     else {
        //         req.continue();
        //     }
        // });
        //iterate over all the ASINS
        for (let i = 0; i < 1; i++) {
            let asin = asins[i];

            //search query with ASIN
            await page.goto(`https://amazon.com/`, {
                waitUntil: "networkidle2",
                timeout: 0,
            });

            console.log(`entered ${asin}`);

            /*
            Note: page.evaluate() lets us run DOM queries and use querySelector functions like we are in the browser console on the website.
            We return the desired output as an object.
            */
            let locationSelector = ''
            if(process.argv[2] =="--device") {locationSelector = '#glow-ingress-block';}else {locationSelector='#glow-ingress-line2';}
            // check location
            // glow-ingress-line2
            let location = await page.evaluate(
                (locationSelector) => {
                    return document.querySelector(locationSelector).innerText;
                },locationSelector);
            console.log(location)
            if (location && location.toLowerCase().indexOf('19706‌') == -1) {
                await page.evaluate(async () => {
                    await fetch("https://www.amazon.com/gp/glow/get-address-selections.html?deviceType=desktop&pageType=Gateway&storeContext=NoStoreName", {
                        "headers": {
                            "accept": "text/html,*/*",
                            "accept-language": "en-US,en;q=0.9,fr;q=0.8,de-AT;q=0.7,de;q=0.6",
                            "downlink": "8.9",
                            "ect": "4g",
                            "rtt": "150",
                            "sec-fetch-dest": "empty",
                            "sec-fetch-mode": "cors",
                            "sec-fetch-site": "same-origin",
                            "x-requested-with": "XMLHttpRequest"
                        },
                        "referrer": "https://www.amazon.com/",
                        "referrerPolicy": "strict-origin-when-cross-origin",
                        "body": null,
                        "method": "GET",
                        "mode": "cors",
                        "credentials": "include"
                    }).then(r => r.text()).then(r => {
                        let doc = new DOMParser().parseFromString(r, "text/html")
                        let token = doc.body.innerText.split('CSRF_TOKEN : "')[1].split('",')[0]
                        //   console.log(token)
                        return fetch("https://www.amazon.com/gp/delivery/ajax/address-change.html", {
                            "headers": {
                                "accept": "text/html,*/*",
                                "accept-language": "en-US,en;q=0.9,fr;q=0.8,de-AT;q=0.7,de;q=0.6",
                                "anti-csrftoken-a2z": token,
                                "content-type": "application/x-www-form-urlencoded",
                                "contenttype": "application/x-www-form-urlencoded;charset=utf-8",
                                "downlink": "8.9",
                                "ect": "4g",
                                "rtt": "150",
                                "sec-fetch-dest": "empty",
                                "sec-fetch-mode": "cors",
                                "sec-fetch-site": "same-origin",
                                "x-requested-with": "XMLHttpRequest"
                            },
                            "referrer": "https://www.amazon.com/",
                            "referrerPolicy": "strict-origin-when-cross-origin",
                            "body": "locationType=LOCATION_INPUT&zipCode=19706&storeContext=generic&deviceType=web&pageType=Gateway&actionSource=glow&almBrandId=undefined",
                            "method": "POST",
                            "mode": "cors",
                            "credentials": "include"
                        })
                    }, console.log);


                })
                const time = 1000;
                await page.waitFor(time);
                await page.goto(`https://amazon.com/`, {
                    waitUntil: "networkidle2",
                    timeout: 0,
                });


            } else {
                const time = 4000;
                await page.waitFor(time);
                console.log(location);


            }
            location = await page.evaluate(
                (locationSelector) => {
                    return document.querySelector(locationSelector).innerText;
                },locationSelector);
            console.log(location)
            
            if(process.argv[2] =="--device"){
                await page.addScriptTag({ path: 'amazonScriptMobile.js' });
                console.log('mobile');
            }else {
                await page.addScriptTag({ url: 'https://code.jquery.com/jquery-3.2.1.min.js' });
            await page.addScriptTag({ path: 'amazonScript.js' });
            }
            let newCart = await page.evaluate(
                async (cart) => {
                    return await Promise.all(cart.map(async item => {
                        let doc = await fetch(item["sellerUrl"]).then(r => r.text()).then(text => new DOMParser().parseFromString(text, 'text/html'))
                        let attr = window.CAPModule.parseProduct(doc)
                        return { ...item, parsed: attr }
                    }))

                }, cart);
            console.log(newCart)
            newCart.reduce((acc, curr) => {
                return acc;
            })
            //extract URL of the product as well as its categories
            const url_and_category = await page.evaluate(
                ({ asin }) => {
                    //extract URL
                    let aNode = document.querySelector(`div[data-asin='${asin}']  a[href]`); // the first a tag with the "data-asin" attribute
                    let url = aNode ? aNode.href : null;

                    //extract category
                    let category, subcategory;
                    let ul = document.querySelector("ul");
                    [category, subcategory] = ul // array destructuring
                        ? Array.from(ul.querySelectorAll("li span.a-list-item")).map(
                            (span) => span.innerText
                        ) //convert ListNode to array, and select all li with a span of class "a-list-item" inside it
                        : [undefined, undefined];

                    return { url: url, category: category, subcategory: subcategory }; //return the object to the url_and_category variable
                },
                { asin }
            );

            console.log(url_and_category);

            //if there was no URL extracted, means that the search query returned no result, which means no such product with ASIN exists
            //then skip this iteration
            if (!url_and_category.url) continue;

            //load up the URL if it was found
            await page.goto(url_and_category.url, {
                waitUntil: "domcontentloaded",
                timeout: 0,
            });

            let data = await page.evaluate(() => {
                //get price
                let priceNode = document.querySelector("span#priceblock_ourprice");
                priceNode = priceNode ? priceNode : document.querySelector("span#priceblock_dealprice");
                let price = priceNode
                    ? parseFloat(priceNode.innerText.split(/\s+/)[1].split(",").join("")) // parse it into float value to avoid commas
                    : "Not Given";

                //get name
                let nameNode = document.querySelector("span#productTitle");
                let name = nameNode
                    ? nameNode.innerText.split(",").join(" ").split("\n").join(" ")
                    : "Not Found"; //split on newline or comma

                //get description
                let descriptionNode = document.querySelector("div#productDescription");
                let description = descriptionNode
                    ? descriptionNode.innerText.split("\n").join(" ").split(",").join(" ") //split on newline or comma
                    : "";

                //get rating
                let ratingsNode = document.querySelector("span[data-hook*=rating]");
                let ratings = ratingsNode ? ratingsNode.innerText.split(" ")[0] + "/5" : "No ratings"; //show the rating in a x/5 form

                //get number of ratings
                let num_ratingsNode = document.querySelector(
                    "div[data-hook='total-review-count'] span"
                );
                let num_ratings = num_ratingsNode ? +num_ratingsNode.innerText.split(" ")[0] : 0;

                //get brand
                let brandNode = document.querySelector("a#bylineInfo");
                let brandArray = brandNode ? brandNode.innerText.split(" ") : [];
                let brand =
                    brandArray.length > 1
                        ? brandArray.slice(1, brandArray.length).join(" ")
                        : "Not Given";

                //attempt to get manufacturer, dimensions, weight, best sellers rank
                let manufacturer, dimensions, weight, best_sellers_rank, category, sub_category; // declare the variables first

                /*
                NOTE: It can be seen that we try to extract category and sub-category here as well as the previous page.evaluate(),
                That is because the best sellers rank in the extra info section often consists of the subcategory, which we can later
                merge/replace/use instead of the category scraped from the search results page, since it is more accurate.
                */

                //2 ways in which information is given. Either in the form of tables, or in the form of Unordered List
                let trs = Array.from(document.querySelectorAll("table[id*=productDetails] tr"));
                let lis = Array.from(document.querySelectorAll("div#detailBullets_feature_div li"));

                //If information is given in the form of table, then gather the list of trs and iterate through them
                if (lis.length == 0 && trs.length > 0) {
                    trs.forEach((tr) => {
                        //each tr in table consists of th and td, parse them
                        //th will be the key(manufacturer, dimensions, etc) and td will contain the value
                        th = tr.querySelector("th").innerText.toLowerCase(); // convert to lowercase in order to avoid case mismatch
                        td = tr.querySelector("td").innerText.toLowerCase();

                        //can extract the value depending on what the key(th) value contains as substring(ex: "Product Manufacturer".includes("manufacturer") = true)
                        if (th) {
                            if (th.includes("manufacturer")) manufacturer = td;
                            else if (th.includes("dimension")) dimensions = td;
                            else if (th.includes("weight")) weight = td;
                            else if (th.includes("best sellers rank")) {
                                best_sellers_rank = td
                                    .split("\n")
                                    .join("AND ")
                                    .replace(/\([^()]*\)/gim, "");
                                if (best_sellers_rank) {
                                    //the rankings are divided by a newline, so split string by newline
                                    [category, sub_category] = td.split("\n").map(
                                        (sentence) =>
                                            sentence
                                                .slice(sentence.indexOf("in") + 3, sentence.length) // Eg: #250 in [category]
                                                .replace(/\([^()]*\)/gim, "") //remove the content between the brackets(including the brackets)
                                                .replace(",", "") //make sure to remove any extra commas
                                    );
                                }
                            }
                        }
                    });
                }
                //If information is given in the form of ul, then parse the li's
                else if (lis.length > 0) {
                    lis.forEach((li) => {
                        //text is in one span, with 2 spans in it, where the first span is the key and the second span is the value
                        //use array destructuring to extract them
                        let [key, val] = Array.from(li.querySelectorAll("span > span")).map((s) =>
                            s.innerText.toLowerCase()
                        );

                        if (key) {
                            //in some cases, instead of the text being in 2 spans within one span, the text is one span directly, so we handle that case here
                            if (!val) val = li.querySelector("span").innerText.split(": ")[1];

                            if (key.includes("manufacturer")) manufacturer = val;
                            else if (key.includes("dimension")) dimensions = val;
                            else if (key.includes("weight")) weight = val;
                            else if (key.includes("best sellers rank")) {
                                best_sellers_rank = val
                                    .split("\n")
                                    .join("AND ")
                                    .replace(/\([^()]*\)/gim, "");
                                if (best_sellers_rank) {
                                    //the rankings are divided by a newline, so split string by newline
                                    [category, sub_category] = val.split("\n").map(
                                        (sentence) =>
                                            sentence
                                                .slice(sentence.indexOf("in") + 3, sentence.length) // Eg: #250 in [category]
                                                .replace(/\([^()]*\)/gim, "") //remove the content between the brackets(including the brackets)
                                                .replace(",", "") //make sure to remove any extra commas
                                    );
                                }
                            }
                        }
                    });
                }

                //get images
                let images = Array.from(document.querySelectorAll("div#altImages img"))
                    .map((img) => (img.src.includes(",") ? "" : img.src)) // make sure to remove imgs with commas in URL
                    .join("  ");

                return {
                    name,
                    price,
                    description,
                    ratings,
                    num_ratings,
                    brand,
                    manufacturer,
                    dimensions,
                    weight,
                    best_sellers_rank,
                    category,
                    sub_category,
                    images,
                };
            });

            // console.log(data);

            //final set of variables to be added to the output,csv file
            let name = data.name;
            let price = data.price;
            let category = data.category ? data.category : url_and_category.category;
            let sub_category = data.sub_category ? data.sub_category : url_and_category.subcategory;
            let images = data.images;
            let description = data.description;
            let ratings = data.ratings;
            let num_ratings = data.num_ratings;
            let brand = data.brand;
            let best_sellers_rank = data.best_sellers_rank
                ? data.best_sellers_rank
                    .split(",") //remove commas for accurate CSV insertion
                    .join("")
                    .slice(0, data.best_sellers_rank.length - 5) //Remove trailing "AND "
                : "Not Given";
            let manufacturer = data.manufacturer ? data.manufacturer.replace(/,/gim, "") : "Not Given"; // remove commas
            let weight = data.weight ? data.weight.replace(/,/gim, "") : "Not Given"; // remove commas
            let dimensions = data.dimensions ? data.dimensions.replace(/,/gim, ";") : "Not Given"; // remove commas

            console.log({
                name,
                price,
                category,
                sub_category,
                images,
                description,
                ratings,
                num_ratings,
                best_sellers_rank,
                brand,
                manufacturer,
                weight,
                dimensions,
            });

            // ASIN,Name,Price,Category,Sub-Category,Images,Description,Rating,Number of Ratings,Best Seller Rank,Brand,Manufacturer,Weight,Dimensions
            //write scraped contents into the CSV File
            fs.appendFileSync(
                "./output.csv",
                `${asin},${name},${price},${category},${sub_category},${images},${description},${ratings},${num_ratings},${best_sellers_rank},${brand},${manufacturer},${weight},${dimensions}\n`
            );
        }

        //close the browser
        // await browser.close();
    } catch (error) {
        console.log(error)
    }
})();