let amazon = (() => {
    const prohibitedcategories = {
        "uk": [],
        "fr": [
            "Auto et Moto › Motos, accessoires et pièces › Vêtements de protection › Protections tête et visage › Masques",
            "Auto et Moto › Motos, accessoires et pièces › Vêtements de protection › Protections tête et visage › Cagoule",
            "Bières, vins et spiritueux",
            "Boissons sans alcool",
            // "Maquillage",
            "Érotisme, Sexe & Sensualité",
            "Sex Toys",
            // "Beauté et Parfum",
            // "Auto et Moto",
            // "Accessoires auto",
            // "Électronique embarquée",
            // "Entretien auto et moto",

            "Huiles et liquides",
            // "Motos, accessoires et pièces",

            "Hygiène et Santé",
            "Epicerie",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Amplificateurs sexuels",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Bien-être et massages érotiques",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Bien-être et massages érotiques > Bougies de massage",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Bien-être et massages érotiques > Crèmes, lotions et huiles de massage",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Bien-être et massages érotiques > Kits de massage érotiques",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Bien-être et massages érotiques > Produits pour le bain",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Chaussures de fétichisme",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Chaussures de fétichisme > Bottes de fétichisme",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Chaussures de fétichisme > Talons hauts de fétichisme",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Contraception et préservatifs",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Contraception et préservatifs > Capes cervicales",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Contraception et préservatifs > Diaphragmes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Contraception et préservatifs > Digues dentaires",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Contraception et préservatifs > Éponges contraceptives",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Contraception et préservatifs > Préservatifs",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Contraception et préservatifs > Préservatifs > Préservatifs féminins",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Contraception et préservatifs > Préservatifs > Préservatifs masculins",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Contraception et préservatifs > Spermicides",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Accessoires",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Femme",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Femme > Catsuits et leggings",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Femme > Corsets et bustiers",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Femme > Culottes et strings",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Femme > Ensembles",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Femme > Jarretières, collants et bas",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Femme > Jupes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Femme > Robes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Femme > Soutiens-gorge",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Homme",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Homme > Chaussettes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Homme > Pantalons",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Homme > Slips et strings",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Homme > Tee-shirts et hauts",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Cuir, latex et vernis > Produits nettoyants et d'entretien",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Jeux et fantaisies érotiques",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Jeux et fantaisies érotiques > Coffrets cadeau et kits érotiques",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Jeux et fantaisies érotiques > Fantaisies",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Jeux et fantaisies érotiques > Jeux érotiques",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Jeux et fantaisies érotiques > Lingerie comestible",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Aphrodisiaques",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Crèmes et gels de toilette intime",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Crèmes pour l'élargissement du sein",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Douches anales et vaginales",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Lubrifiants",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Nettoyants pour sextoys",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Peinture pour le corps",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Pompes à pénis",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Pompes à seins",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Produits de virilité et retardants",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Relâchement du sphincter",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Serrage vaginal",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Lubrifiants, stimulants et hygiène > Tampons éponge",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Mobilier érotique",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Mobilier érotique > Balançoires",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Mobilier érotique > Barres de strip tease",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Mobilier érotique > Coussins",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Mobilier érotique > Draps",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Mobilier érotique > Matelas",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Mobilier érotique > Poignées et supports",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Mobilier érotique > Sacs de sauna",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Anneaux de pénis",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Boules de geisha",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Cathéter et sonde urinaire",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Coffrets et sets",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Couches érotiques",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Dilatateurs",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Doigts chinois",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Electro-stimulation",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Fesser et taquiner",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Fesser et taquiner > Bâtons",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Fesser et taquiner > Caresses",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Fesser et taquiner > Cravaches",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Fesser et taquiner > Fouets et flagellateurs",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Fesser et taquiner > Fouets et flagellateurs > Flagellateurs",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Fesser et taquiner > Fouets et flagellateurs > Fouets",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Fesser et taquiner > Tapettes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Gaines pour pénis",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Godemichets",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Godes ceintures",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Jouets de mamelon",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Jouets de mamelon > Pinces",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Jouets de mamelon > Poids",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Jouets de mamelon > Suceurs",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Kits de moulage",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Machines et appareils",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Masseurs de prostate",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Masseurs pour les seins",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Masturbateurs masculins",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Masturbateurs masculins > Masturbateurs cup et accessoires",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Masturbateurs masculins > Masturbateurs cup et accessoires > Accessoires",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Masturbateurs masculins > Masturbateurs cup et accessoires > Appareils masturbateurs cup",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Masturbateurs masculins > Masturbateurs cup et accessoires > Masturbateurs cup manuels",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Masturbateurs masculins > Masturbateurs oeufs",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Masturbateurs masculins > Masturbateurs réalistes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Poupées d’amour",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Bâillons",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Bandeaux",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Cages de chasteté",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Cagoules",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Ceintures de chasteté",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Colliers",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Contraintes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Cordes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Foulards",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Harnais",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Kits de bondage",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Menottes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Retenue et bondage > Scotchs de bondage",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Sex Toys anals",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Sex Toys anals > Perles anales",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Sex Toys anals > Plugs anals",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Suceurs de clitoris",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Tiges chauffantes",
            "Hygiène et Santé > Érotisme, sexe et sensualité > Sex Toys > Vibromasseurs",
            "Sports et Loisirs > Tir à l'arc > Accessoires et Maintenance",
            "Sports et Loisirs > Tir à l'arc > Arcs",
            "Sports et Loisirs > Tir à l'arc > Carquois",
            "Sports et Loisirs > Tir à l'arc > Cibles",
            "Sports et Loisirs > Tir à l'arc > Collets d'arcs",
            "Sports et Loisirs > Tir à l'arc > Cordes d'arbalète",
            "Sports et Loisirs > Tir à l'arc > Cordes d'arcs",
            "Sports et Loisirs > Tir à l'arc > Encoches",
            "Sports et Loisirs > Tir à l'arc > Flèches",
            "Sports et Loisirs > Tir à l'arc > Flèches > Comptage des points",
            "Sports et Loisirs > Tir à l'arc > Flèches > Flèches de sport",
            "Sports et Loisirs > Tir à l'arc > Flèches > Flèches pour arbalète",
            "Sports et Loisirs > Tir à l'arc > Flèches > Flèches pour la chasse",
            "Sports et Loisirs > Tir à l'arc > Flèches > Pointes de flèche",
            "Sports et Loisirs > Tir à l'arc > Kits",
            "Sports et Loisirs > Tir à l'arc > Lunettes de tir pour arbalètes",
            "Sports et Loisirs > Tir à l'arc > Nasettes",
            "Sports et Loisirs > Tir à l'arc > Plumes",
            "Sports et Loisirs > Tir à l'arc > Protections",
            "Sports et Loisirs > Tir à l'arc > Protections main et poignet",
            "Sports et Loisirs > Tir à l'arc > Release Aids",
            "Sports et Loisirs > Tir à l'arc > Repose flèche",
            "Sports et Loisirs > Tir à l'arc > Sacs d'arcs",
            "Sports et Loisirs > Tir à l'arc > Stabilisateurs",
            "Sports et Loisirs > Tir à l'arc > Visières",
            "Sports et Loisirs›Camping et randonnée›Couteaux et outils›Couteaux à lame fixe",
            "Sports et Loisirs>Camping et randonnée>Couteaux et outils",
            "Sports et Loisirs>Camping et randonnée>Couteaux et outils>Couteaux pliants",
            "Jeux et Jouets>Déguisements et accessoires>Accessoires>Armes",
            "Jeux et Jouets>Déguisements et accessoires>Accessoires>Armes>Boucliers",
            "Jeux et Jouets>Déguisements et accessoires>Accessoires>Armes>Épées",
            "Jeux et Jouets>Déguisements et accessoires>Accessoires>Armes>Haches de combat",
            "Jeux et Jouets>Déguisements et accessoires>Accessoires>Armes>Lances",
            "Jeux et Jouets>Déguisements et accessoires>Accessoires>Armes>Pistolets",
            "Jeux et Jouets>Déguisements et accessoires>Accessoires>Armes>Sabres",
            "Sports et Loisirs>Chasse",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Accessoires de crosse",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Bobines",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Canons",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Cartouchières",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Chargeurs",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Crosses",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Gâchettes",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Outillage d'armurerie",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Pieds de visée",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Pivotants",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Poignées",
            "Sports et Loisirs>Chasse>Accessoires d'armes>Supports pour arme",
            "Sports et Loisirs>Chasse>Fusils à air comprimé",
            "Sports et Loisirs>Chasse>Optique et orientation>Lasers",
            "Sports et Loisirs>Escrime>Armes",
            "Sports et Loisirs>Escrime>Armes>Epées",
            "Sports et Loisirs>Escrime>Armes>Epées>Armes complètes",
            "Sports et Loisirs>Escrime>Armes>Fleurets",
            "Sports et Loisirs>Escrime>Armes>Fleurets>Pièces détachées",
            "Sports et Loisirs>Escrime>Armes>Sabres",
            "Sports et Loisirs>Escrime>Armes>Sabres>Armes complètes",
            "Sports et Loisirs>Escrime>Armes>Sabres>Pièces détachées",
            "Sports et Loisirs>Sports de combat>Armes",
            "Sports et Loisirs>Sports de combat>Armes>Armes de ninja",
            "Sports et Loisirs>Sports de combat>Armes>Bâtons",
            "Sports et Loisirs>Sports de combat>Armes>Bâtons d'Arnis",
            "Sports et Loisirs>Sports de combat>Armes>Bokken",
            "Sports et Loisirs>Sports de combat>Armes>Couteaux",
            "Sports et Loisirs>Sports de combat>Armes>Epées",
            "Sports et Loisirs>Sports de combat>Armes>Epées d'entraînement",
            "Sports et Loisirs>Sports de combat>Armes>Kamas",
            "Sports et Loisirs>Sports de combat>Armes>Matraques",
            "Sports et Loisirs>Sports de combat>Armes>Nunchakus",
            "Sports et Loisirs>Sports de combat>Armes>Sais",
            "Sports et Loisirs>Sports de combat>Armes>Tonfas",
            "Sports et Loisirs>Sports de combat>Articles d'entraînement",
            "Sports et Loisirs > Airsoft > Balles",
            "Sports et Loisirs > Airsoft > Batteries",
            "Sports et Loisirs > Airsoft > Billes",
            "Sports et Loisirs > Airsoft > Chargeurs",
            "Sports et Loisirs > Airsoft > Chargeurs batterie",
            "Sports et Loisirs > Airsoft > Cibles",
            "Sports et Loisirs > Airsoft > Fixations de lunettes de tir",
            "Sports et Loisirs > Airsoft > Gilets tactiques",
            "Sports et Loisirs > Airsoft > Grenades",
            "Sports et Loisirs > Airsoft > Gun Loaders",
            "Sports et Loisirs > Airsoft > Holsters",
            "Sports et Loisirs > Airsoft > Lasers",
            "Sports et Loisirs > Airsoft > Lunettes de visée",
            "Sports et Loisirs > Airsoft > Mallettes et housses",
            "Sports et Loisirs > Airsoft > Outils",
            "Sports et Loisirs > Airsoft > Pistolets et fusils",
            "Sports et Loisirs > Airsoft > Pistolets et fusils > Carabines",
            "Sports et Loisirs > Airsoft > Pistolets et fusils > Fusils à grenaille",
            "Sports et Loisirs > Airsoft > Pistolets et fusils > Pistolets",
            "Sports et Loisirs > Airsoft > Protections",
            "Sports et Loisirs > Airsoft > Rails",
            "Sports et Loisirs > Airsoft > Sets",
            "Sports et Loisirs > Airsoft > Tenue de camouflage",
            "Sports et Loisirs > Airsoft > Visières",
            "Commerce, Industrie et Science > Produits scientifiques et de laboratoire > Produits chimiques de laboratoire",
           /* "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud",
            "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud > Blanchisseurs à café",
            "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud > Café et expresso",
            "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud > Sirops de café",
            "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud > Substituts de café",
            "Epicerie > Confiseries, chocolats et chewing-gums",
            "Epicerie > Confiseries, chocolats et chewing-gums > Assortiments de confiseries",
            "Epicerie > Confiseries, chocolats et chewing-gums > Berlingots",
            "Epicerie > Confiseries, chocolats et chewing-gums > Bonbon au caramel",
            "Epicerie > Confiseries, chocolats et chewing-gums > Bonbons acides",
            "Epicerie > Confiseries, chocolats et chewing-gums > Bonbons et chocolat au bacon",
            "Epicerie > Confiseries, chocolats et chewing-gums > Caramel au beurre",
            "Epicerie > Confiseries, chocolats et chewing-gums > Caramel mou",
            "Epicerie > Confiseries, chocolats et chewing-gums > Caramels",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chewing-gum et bubble-gum",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chewing-gum et bubble-gum > Bonbons gélifiés",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chewing-gum et bubble-gum > Bubble-gum",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Assortiments de chocolat et échantillons",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Barres et tablettes",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Bretzels au chocolat",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Friandises au chocolat",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Fruits chocolatés",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Grains de café chocolatés",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Noix chocolatées",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Œuf en chocolat",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Personnages",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Pralines et truffes",
            "Epicerie > Confiseries, chocolats et chewing-gums > Chocolat > Sachets et boîtes",
            "Epicerie > Confiseries, chocolats et chewing-gums > Coffrets de chocolat et confiserie",
            "Epicerie > Confiseries, chocolats et chewing-gums > Confiseries au gingembre",
            "Epicerie > Confiseries, chocolats et chewing-gums > Confiseries fruitées",
            "Epicerie > Confiseries, chocolats et chewing-gums > Dragées",
            "Epicerie > Confiseries, chocolats et chewing-gums > Drops",
            "Epicerie > Confiseries, chocolats et chewing-gums > Halva",
            "Epicerie > Confiseries, chocolats et chewing-gums > Menthe",
            "Epicerie > Confiseries, chocolats et chewing-gums > Nougat",
            "Epicerie > Confiseries, chocolats et chewing-gums > Réglisse",
            "Epicerie > Confiseries, chocolats et chewing-gums > Sachets festifs",
            "Epicerie > Confiseries, chocolats et chewing-gums > Sucettes",
            "Epicerie > Entretien de la maison et nettoyage",
            "Epicerie > Paniers gourmands et coffrets cadeaux gourmets > Chocolats",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Chocolats et graines de caroube à cuire > Farine de caroube",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Graines de caroube",
            "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud > Chocolat chaud et boissons à base de malt",
            "Epicerie > Ingrédients de cuisine et pâtisserie",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Chapelures et pâtes à frire",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Chocolats et graines de caroube à cuire",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Chocolats et graines de caroube à cuire > Cacao",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Chocolats et graines de caroube à cuire > Chocolats",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Chocolats et graines de caroube à cuire > Pépites de chocolats",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Décorations pour gâteaux et desserts",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Enrobages et pâtes",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Epaississants",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Fonds de tarte",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Garnitures à pâtisserie",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Arômes",
            "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud > Chocolat chaud et boissons à base de malt > Boisson à base de malt",
            "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud > Chocolat chaud et boissons à base de malt > Cacao",
            "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud > Chocolat chaud et boissons à base de malt > Chocolat chaud",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Guimauves",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Huiles, vinaigres et sprays",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Farines",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Laits concentrés et laits en poudre",
            "Epicerie > Alimentation bébé",
            "Epicerie > Alimentation bébé > Autres boissons et smoothies",
            "Epicerie > Alimentation bébé > Céréales et bouillies",
            "Epicerie > Alimentation bébé > Céréales et bouillies > Bouillies",
            "Epicerie > Alimentation bébé > Céréales et bouillies > Céréales",
            "Epicerie > Alimentation bébé > Goûters",
            "Epicerie > Alimentation bébé > Goûters > Autres goûters",
            "Epicerie > Alimentation bébé > Goûters > Barres aux fruits",
            "Epicerie > Alimentation bébé > Goûters > Biscuits et biscottes",
            "Epicerie > Alimentation bébé > Lait bébé",
            "Epicerie > Alimentation bébé > Repas",
            "Epicerie > Alimentation bébé > Repas > Desserts mixtes",
            "Epicerie > Alimentation bébé > Repas > Dîners",
            "Epicerie > Alimentation bébé > Repas > Fruits et desserts",
            "Epicerie > Alimentation bébé > Repas > Légumes",
            "Epicerie > Alimentation bébé > Repas > Soupes",
            "Epicerie > Alimentation bébé > Repas > Viandes et poissons",
            "Epicerie > Céréales et Muesli",
            "Epicerie > Céréales et Muesli > Barres de céréales et biscuits",
            "Epicerie > Céréales et Muesli > Barres de céréales et biscuits > Barres de céréales",
            "Epicerie > Céréales et Muesli > Barres de céréales et biscuits > Barres nutritives et énergétiques",
            "Epicerie > Céréales et Muesli > Céréales de riz",
            "Epicerie > Céréales et Muesli > Céréales froides",
            "Epicerie > Céréales et Muesli > Céréales pour enfants",
            "Epicerie > Céréales et Muesli > Céréales riches en fibre",
            "Epicerie > Céréales et Muesli > Flocons d'avoine et porridge",
            "Epicerie > Céréales et Muesli > Granolas",
            "Epicerie > Céréales et Muesli > Grits",
            "Epicerie > Céréales et Muesli > Muesli",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Décorations culinaires",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Décorations culinaires > Décorations pour gâteaux",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Décorations culinaires > Encres et papiers comestibles",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Décorations culinaires > Encres et papiers comestibles > Encres comestibles",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Décorations culinaires > Encres et papiers comestibles > Papiers comestibles",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Décorations culinaires > Kits de décoration",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Décorations culinaires > Paillettes comestibles",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Décorations culinaires > Poudres comestibles",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Décorations culinaires > Sucres décoratifs",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Glaçages",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Nappage, glaçage et décorations > Nappage",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Noix de coco râpée",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Noris",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Œufs déshydratés",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Pâtes à pâtisserie",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Préparation pour crème anglaise",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Préparation pour pâtes de patisserie",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Préparations de pudding et gélatine",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Préparations de pudding et gélatine > Gélatine",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Préparations de pudding et gélatine > Pectines",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Préparations de pudding et gélatine > Pudding",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Saindoux et graisses alimentaires",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops et sauces pour desserts",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops et sauces pour desserts > Caramel",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops et sauces pour desserts > Chocolat",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops et sauces pour desserts > Fruit",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops et sauces pour desserts > Guimauve",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops et sauces pour desserts > Sirops aromatisés",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops, sucres et édulcorants",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops, sucres et édulcorants > Mélasse",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops, sucres et édulcorants > Miel",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops, sucres et édulcorants > Sirop d'agave",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops, sucres et édulcorants > Sirop d'érable",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops, sucres et édulcorants > Sirops natures",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Sirops, sucres et édulcorants > Sucres",
            "Epicerie > Ingrédients de cuisine et pâtisserie > Vins culinaires",
            "Epicerie > Snacks",
            "Epicerie > Snacks > Assortiment festif",
            "Epicerie > Snacks > Biscuits",
            "Epicerie > Snacks > Biscuits soufflés",
            "Epicerie > Snacks > Bretzels et sticks de bretzels",
            "Epicerie > Snacks > Chips",
            "Epicerie > Snacks > Compotes",
            "Epicerie > Snacks > Cornets de glace et décorations",
            "Epicerie > Snacks > Couenne de porc",
            "Epicerie > Snacks > Crackers",
            "Epicerie > Snacks > Cuir de fruit",
            "Epicerie > Snacks > Galettes de riz, chips et crackers",
            "Epicerie > Snacks > Galettes de riz, chips et crackers > Chips",
            "Epicerie > Snacks > Galettes de riz, chips et crackers > Crackers",
            "Epicerie > Snacks > Galettes de riz, chips et crackers > Galettes",
            "Epicerie > Snacks > Mélanges de graines et raisins",
            "Epicerie > Snacks > Mélanges de graines et raisins > Bombay Mix",
            "Epicerie > Snacks > Mélanges de graines et raisins > Fruits secs et graines",
            "Epicerie > Snacks > Muesli et barres chocolatées",
            "Epicerie > Snacks > Pois wasabi",
            "Epicerie > Snacks > Popcorn",
            "Epicerie > Snacks > Popcorn > Eclaté",
            "Epicerie > Snacks > Popcorn > Micro-ondes",
            "Epicerie > Snacks > Popcorn > Non éclaté",
            "Epicerie > Snacks > Pudding et gélatine",
            "Epicerie > Snacks > Sauces Salsa, dips et tartinables",
            "Epicerie > Snacks > Sauces Salsa, dips et tartinables > Dips et tartinades",
            "Epicerie > Snacks > Sauces Salsa, dips et tartinables > Sauces Salsa",
            "Epicerie > Snacks > Snacks de fruits",
            "Epicerie > Snacks > Snacks de légumes",
            "Epicerie > Snacks > Snacks traditionnels",
            "Epicerie > Café, thé et boissons > Café, thé et chocolat chaud > Thé et infusions",
            "Epicerie > Café, thé et boissons > Thé aux perles",
            "Epicerie > Café, thé et boissons > Thé et citronnade glacés",
            "Epicerie > Café, thé et boissons > Thé et citronnade glacés > Citronnade",
            "Epicerie > Café, thé et boissons > Thé et citronnade glacés > Thés glacés",
            "Epicerie > Bières, vins et spiritueux > Vins liquoreux et vins de dessert",
            "Epicerie > Paniers gourmands et coffrets cadeaux gourmets > Charcuteries",
            "Epicerie > Huiles, vinaigres et sauces salade",
            "Epicerie > Huiles, vinaigres et sauces salade > Garniture",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Amande douce",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Arachide",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Avocat",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Cacahuète",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Carthame",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Colza",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Graines de lin",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Maïs",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Mélange",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Moutarde",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Noisette",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Noix",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Noix de coco",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Olive",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Palme",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Pépin de raisin",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Sésame",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Soja",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Son de riz",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Tournesol",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Truffe",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Végétales",
            "Epicerie > Huiles, vinaigres et sauces salade > Sauces salade",
            "Epicerie > Huiles, vinaigres et sauces salade > Sprays alimentaires",
            "Epicerie > Légumes secs, riz et farine",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Avoine cultivée",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Blé",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Éleusine (ragi, millet)",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Épeautre",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Mil à chandelle",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Quinoa",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Riz sauvage",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Riz thaï",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Sarrasin",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Sorgho",
            "Epicerie > Légumes secs, riz et farine > Légumes secs",
            "Epicerie > Légumes secs, riz et farine > Riz",
            "Epicerie > Légumes secs, riz et farine > Riz > Flocons de riz ou riz aplati",
            "Epicerie > Légumes secs, riz et farine > Riz > Idli et riz Dosa",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Arborio",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Basmati",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz blanc",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz brun",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Ponni",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz rouge",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz rouge de Matta",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Sona Masoori",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Surti Kolam",
            "Epicerie > Légumes secs, riz et farine > Sago",
            "Epicerie > Huiles, vinaigres et sauces salade",
            "Epicerie > Huiles, vinaigres et sauces salade > Garniture",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Amande douce",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Arachide",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Avocat",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Cacahuète",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Carthame",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Colza",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Graines de lin",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Maïs",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Mélange",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Moutarde",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Noisette",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Noix",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Noix de coco",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Olive",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Palme",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Pépin de raisin",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Sésame",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Soja",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Son de riz",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Tournesol",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Truffe",
            "Epicerie > Huiles, vinaigres et sauces salade > Huiles > Végétales",
            "Epicerie > Huiles, vinaigres et sauces salade > Sauces salade",
            "Epicerie > Huiles, vinaigres et sauces salade > Sprays alimentaires",
            "Epicerie > Légumes secs, riz et farine",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Avoine cultivée",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Blé",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Éleusine (ragi, millet)",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Épeautre",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Mil à chandelle",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Quinoa",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Riz sauvage",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Riz thaï",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Sarrasin",
            "Epicerie > Légumes secs, riz et farine > Céréales complètes > Sorgho",
            "Epicerie > Légumes secs, riz et farine > Légumes secs",
            "Epicerie > Légumes secs, riz et farine > Riz",
            "Epicerie > Légumes secs, riz et farine > Riz > Flocons de riz ou riz aplati",
            "Epicerie > Légumes secs, riz et farine > Riz > Idli et riz Dosa",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Arborio",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Basmati",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz blanc",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz brun",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Ponni",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz rouge",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz rouge de Matta",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Sona Masoori",
            "Epicerie > Légumes secs, riz et farine > Riz > Riz Surti Kolam",
            "Epicerie > Légumes secs, riz et farine > Sago",
            "Epicerie > Conserves et bocaux",
            "Epicerie > Conserves et bocaux > Bouillons et broths",
            "Epicerie > Conserves et bocaux > Bouillons et broths > Bouillions",
            "Epicerie > Conserves et bocaux > Bouillons et broths > Bouillions > Bœuf",
            "Epicerie > Conserves et bocaux > Bouillons et broths > Bouillions > Légumes",
            "Epicerie > Conserves et bocaux > Bouillons et broths > Bouillions > Poulet",
            "Epicerie > Conserves et bocaux > Bouillons et broths > Broths",
            "Epicerie > Conserves et bocaux > Bouillons et broths > Stocks",
            "Epicerie > Laitages et produits frais",
            "Epicerie > Laitages et produits frais > Boissons à base de laitage",
            "Epicerie > Laitages et produits frais > Boissons à base de laitage > Babeurre",
            "Epicerie > Laitages et produits frais > Boissons à base de laitage > Boissons instantanées pour petit déjeuner",
            "Epicerie > Laitages et produits frais > Boissons à base de laitage > Lait aromatisé et lassi",
            "Epicerie > Laitages et produits frais > Boissons à base de laitage > Milk-shakes",
            "Epicerie > Laitages et produits frais > Lait de vache",
            "Hygiène et Santé > Bien-être et massage > Massage et relaxation > Masseurs électriques > Masseurs électriques portables",
            "Hygiène et Santé > Nutrition et Diététique > Amaigrissement et perte de poids",
            "Hygiène et Santé > Nutrition et Diététique > Amaigrissement et perte de poids > Appareils d'aide à la perte de poids",
            "Hygiène et Santé > Nutrition et Diététique > Amaigrissement et perte de poids > Barres diététiques",
            "Hygiène et Santé > Nutrition et Diététique > Amaigrissement et perte de poids > Compléments",
            "Hygiène et Santé > Nutrition et Diététique > Amaigrissement et perte de poids > Coupe-faims",
            "Hygiène et Santé > Nutrition et Diététique > Amaigrissement et perte de poids > Diurétiques",
            "Hygiène et Santé > Nutrition et Diététique > Amaigrissement et perte de poids > Shakes diététiques",
            "Hygiène et Santé > Nutrition et Diététique > Amaigrissement et perte de poids > Snacks diététiques",
            "Hygiène et Santé > Nutrition et Diététique > Barres et boissons nutritives",
            "Hygiène et Santé > Nutrition et Diététique > Barres et boissons nutritives > Barres nutritives",
            "Hygiène et Santé > Nutrition et Diététique > Barres et boissons nutritives > Boissons et shakes nutritifs",
            "Hygiène et Santé > Nutrition et Diététique > Barres et boissons nutritives > Gels nutritifs et tablettes",
            "Hygiène et Santé > Nutrition et Diététique > Compléments alimentaires pour sportifs",
            "Hygiène et Santé > Nutrition et Diététique > Compléments alimentaires pour sportifs > Shakers pour compléments alimentaires pour sportifs",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acide citritique",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acide hyaluronique",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > Acétyl-L-carnitine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > BCAA",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > Bêta-alanine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > Carnitine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > Créatine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > L-Arginine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > L-Citrulline",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > L-Glutamine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > L-Leucine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > L-Lysine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > L-Ornithine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides aminés > Taurine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > 7-Keto",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > ALC",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Brûleurs de graisse",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Combinaisons",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > DHA",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > EPA",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Huile de foie de morue",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Huiles de cassis",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Huiles de krill",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Huiles de lin",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Huiles de poisson",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Oméga",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Oméga > Oméga 3-6-9",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Oméga > Oméga-3",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Oméga > Omega-6",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Acides gras essentiels > Oméga > Oméga-9",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Acide alpha-lipoïque",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Astaxanthine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Bêta-Carotène",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Caroténoïdes",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Coenzyme Q10",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > DMAE",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Lutéine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Lycopène",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Multi-antioxydants",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Polyphénol",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Polyphénol > Flavonoïdes",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Polyphénol > Flavonoïdes > Bio-flavonoïdes",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Polyphénol > Flavonoïdes > Catéchine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Polyphénol > Flavonoïdes > Isoflavones",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Polyphénol > Flavonoïdes > Quercétine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Polyphénol > Flavonoïdes > Rutoside",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Pycnogénol",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Antioxydants > Resvératrol",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Bactéries et levures",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Chondrotoïne & glucosamine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Chondrotoïne & glucosamine > Chondrotoïne",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Chondrotoïne & glucosamine > Glucosamine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Chondrotoïne & glucosamine > Mélange",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Actée à grappes noires",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Ail",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Alfalfa",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Aloe Vera",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Angélique chinoise",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Artichaut",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Astragale",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Aubépine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Boissons vertes",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Café vert",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Camomille",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Cannelle",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Cayenne",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Champignons",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Chardon-marie",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Chlorelle",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Chlorophylle",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Curcuma",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Curcumin",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Echinacée",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits > Baie d'açaï",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits > Canneberge",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits > Cerise",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits > Cétones de framboise",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits > Grenade",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits > Lucuma",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits > Myrtille et bleuet",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits > Pépin de raisin",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Extraits de fruits > Sureau",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Feuille d'olivier",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Garcinia cambogia",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Gingembre",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Ginkgo biloba",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Ginseng",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Graine de Lin",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Graines de chia",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Grande camomille",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Grande ortie",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Herbe de blé",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Hydraste du Canada",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Kelp",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Levure de riz rouge",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Maca",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Millepertuis perforé",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Muira puama",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Neem",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Noni",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Onagre",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Orégan",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Palmier scie",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Psyllium",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Racine de guimauve",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Réglisse",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Sésamine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Spiruline",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Stévia",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Thé vert",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Tribulus",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Triphala",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments à base de plantes > Valériane",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Mélange de protéines",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines de bœuf",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines de caséine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines de chanvre",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines de chèvre",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines de plantes",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines de pois",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines de riz",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines de soja",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines d'œuf",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines végétariennes",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Compléments alimentaires protéinés > Protéines whey",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Enzymes",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Extraits glandulaires",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Extraits glandulaires > Extraits de cortex surrénalien",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Extraits glandulaires > Extraits de foie",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Extraits glandulaires > Extraits de thymus",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Extraits glandulaires > Placenta",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Fibre alimentaire",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Gelée royale",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Levure de bière",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Lipide",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Lipide > Céramide",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Lipide > Choline",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Lipide > Lécithine",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Calcium",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Chrome",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Cuivre",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Fer",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Iode",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Iodure de potassium",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Magnésium",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Manganèse",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Multi-minéraux",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Oligo-éléments",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Potassium",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Sélénium",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Sodium",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Minéraux > Zinc",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > MSM",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Multi-vitamines et minéraux",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Propolis",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vinaigre de cidre",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Multi-vitamines",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine A",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Complexe de Vitamine B",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Vitamine B1",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Vitamine B12",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Vitamine B2",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Vitamine B3",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Vitamine B5",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Vitamine B6",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Vitamine B7 (Biotine)",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Vitamine B8",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine B > Vitamine B9 (acide folique)",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine C",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine D",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine E",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamine K",
            "Hygiène et Santé > Nutrition et Diététique > Vitamines, minéraux et compléments > Vitamines > Vitamines prénatales"
            , "Hygiène et Santé > Nutrition et Diététique"
            , "Sports et Loisirs › Camping et randonnée › Cuisine",
            
            "Beauté et Parfum > Bain, savons et soins du corps",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain > Bonnets de douche",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain > Brosses",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain > Coussins de bain",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain > Fleurs de bain",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain > Gants et carrés de toilette",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain > Gants exfoliants",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain > Loofahs",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain > Ponts de baignoire",
            "Beauté et Parfum > Bain, savons et soins du corps > Accessoires pour le bain > Éponges",
            "Beauté et Parfum > Bain, savons et soins du corps > Coffrets cadeaux",
            "Beauté et Parfum > Bain, savons et soins du corps > Déodorants et anti-transpirants",
            "Beauté et Parfum > Bain, savons et soins du corps > Produits pour le bain",
            "Beauté et Parfum > Bain, savons et soins du corps > Produits pour le bain > Bains moussants",
            "Beauté et Parfum > Bain, savons et soins du corps > Produits pour le bain > Boules effervescentes",
            "Beauté et Parfum > Bain, savons et soins du corps > Produits pour le bain > Huiles de bain",
            "Beauté et Parfum > Bain, savons et soins du corps > Produits pour le bain > Infusions de bain",
            "Beauté et Parfum > Bain, savons et soins du corps > Produits pour le bain > Perles et confettis",
            "Beauté et Parfum > Bain, savons et soins du corps > Produits pour le bain > Sels de bain",
            "Beauté et Parfum > Bain, savons et soins du corps > Savons et gels douche",
            "Beauté et Parfum > Bain, savons et soins du corps > Savons et gels douche > Gels douche",
            "Beauté et Parfum > Bain, savons et soins du corps > Savons et gels douche > Huiles de douche",
            "Beauté et Parfum > Bain, savons et soins du corps > Savons et gels douche > Savons et nettoyants pour les mains",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages > Argiles",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages > Beurres",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages > Brumes corporelles parfumées",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages > Crèmes et laits",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages > Gommages",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages > Huiles",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages > Lotions",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages > Mousses",
            "Beauté et Parfum > Bain, savons et soins du corps > Soins du corps et gommages > Poudres de talc",
          /*  "Beauté et Parfum > Coiffure et soins des cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Accessoires à chignons",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Bandeaux, headbands et serre-têtes",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Barrettes",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Elastiques",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Extensions de cheveux, perruques et accessoires",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Extensions de cheveux, perruques et accessoires > Extensions de cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Extensions de cheveux, perruques et accessoires > Perruques",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Extensions de cheveux, perruques et accessoires > Porte perruques",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Extensions de cheveux, perruques et accessoires > Postiches",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Peignes à cheveux décoratifs",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Pinces à cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Accessoires de coiffure > Piques et épingles à cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Appareils multifonctionnels",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Bigoudis",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Brosses",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Brosses électriques et soufflantes",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Casques chauffants",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Ciseaux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Fers à boucler",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Fers à gaufrer",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Fers à lisser",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Fers à onduler",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Peignes",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Sèche-cheveux et accessoires",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Sèche-cheveux et accessoires > Diffuseurs",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Sèche-cheveux et accessoires > Embouts peignes",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Sèche-cheveux et accessoires > Sèche-cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Appareils et outils de coiffure > Tondeuses à cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Colorations",
            "Beauté et Parfum > Coiffure et soins des cheveux > Colorations > Accessoires pour colorations",
            "Beauté et Parfum > Coiffure et soins des cheveux > Colorations > Coloration permanente",
            "Beauté et Parfum > Coiffure et soins des cheveux > Colorations > Coloration semi-permanente",
            "Beauté et Parfum > Coiffure et soins des cheveux > Colorations > Produits éclaircissants et décolorants",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Bacs à shampoing",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Balais et brosses",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Balais à cou",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Capes de coupe",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Casques chauffants",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Chariots",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Fauteuils de coiffure",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Miroirs à main",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Tabourets de coiffure",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Têtes d'exercice",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Valises, trousses et vanitys",
            "Beauté et Parfum > Coiffure et soins des cheveux > Mobilier et matériel pour salons de coiffure > Vaporisateurs vides",
            "Beauté et Parfum > Coiffure et soins des cheveux > Produits coiffants",
            "Beauté et Parfum > Coiffure et soins des cheveux > Produits coiffants > Gels",
            "Beauté et Parfum > Coiffure et soins des cheveux > Produits coiffants > Laques et sprays",
            "Beauté et Parfum > Coiffure et soins des cheveux > Produits coiffants > Mousses",
            "Beauté et Parfum > Coiffure et soins des cheveux > Produits coiffants > Sprays thermo-protecteurs",
            "Beauté et Parfum > Coiffure et soins des cheveux > Produits coiffants > Sérums",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Après-shampooings",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Coffrets cadeaux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Défrisants",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Huiles pour cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Kits de voyage",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Lotions toniques pour cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Mascaras pour cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Permanentes et texturisants",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Produits contre la perte de cheveux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Sets de shampooings et après-shampooings",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Shampooings",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Shampooings secs",
            "Beauté et Parfum > Coiffure et soins des cheveux > Shampooings et soins > Soins et masques",
            "Beauté et Parfum > Coiffure et soins des cheveux > Traitements anti-poux",
            "Beauté et Parfum > Coiffure et soins des cheveux > Traitements anti-poux > Peignes à poux",
            "Beauté et Parfum > Maquillage",
            "Beauté et Parfum > Maquillage > Coffrets de maquillage",
            "Beauté et Parfum > Maquillage > Corps",
            "Beauté et Parfum > Maquillage > Corps > Anti-tâches et correcteurs",
            "Beauté et Parfum > Maquillage > Corps > Henné",
            "Beauté et Parfum > Maquillage > Corps > Paillettes",
            "Beauté et Parfum > Maquillage > Corps > Peinture corporelle",
            "Beauté et Parfum > Maquillage > Corps > Tatouages temporaires",
            "Beauté et Parfum > Maquillage > Démaquillants",
            "Beauté et Parfum > Maquillage > Démaquillants > Démaquillants pour le visage",
            "Beauté et Parfum > Maquillage > Démaquillants > Démaquillants pour les yeux",
            "Beauté et Parfum > Maquillage > Lèvres",
            "Beauté et Parfum > Maquillage > Lèvres > Bases de maquillage pour les lèvres",
            "Beauté et Parfum > Maquillage > Lèvres > Baumes teintés et vernis à lèvres",
            "Beauté et Parfum > Maquillage > Lèvres > Crayons à lèvres",
            "Beauté et Parfum > Maquillage > Lèvres > Gloss",
            "Beauté et Parfum > Maquillage > Lèvres > Rouges à lèvres",
            "Beauté et Parfum > Maquillage > Palettes de maquillage",
            "Beauté et Parfum > Maquillage > Teint",
            "Beauté et Parfum > Maquillage > Teint > Anti-tâches et correcteurs",
            "Beauté et Parfum > Maquillage > Teint > BB et CC crèmes",
            "Beauté et Parfum > Maquillage > Teint > Bases de maquillage",
            "Beauté et Parfum > Maquillage > Teint > Blush et fards à joues",
            "Beauté et Parfum > Maquillage > Teint > Enlumineurs et illuminateurs",
            "Beauté et Parfum > Maquillage > Teint > Fonds de teint",
            "Beauté et Parfum > Maquillage > Teint > Poudres",
            "Beauté et Parfum > Maquillage > Teint > Poudres de finition et fixateurs",
            "Beauté et Parfum > Maquillage > Teint > Poudres de soleil et bronzantes",
            "Beauté et Parfum > Maquillage > Yeux",
            "Beauté et Parfum > Maquillage > Yeux > Anti-cernes et correcteurs",
            "Beauté et Parfum > Maquillage > Yeux > Crayons et khôls yeux",
            "Beauté et Parfum > Maquillage > Yeux > Crayons et maquillage pour sourcils",
            "Beauté et Parfum > Maquillage > Yeux > Crayons fards à paupières",
            "Beauté et Parfum > Maquillage > Yeux > Eyeliners",
            "Beauté et Parfum > Maquillage > Yeux > Fard à paupières",
            "Beauté et Parfum > Maquillage > Yeux > Fards à paupières",
            "Beauté et Parfum > Maquillage > Yeux > Faux-cils",
            "Beauté et Parfum > Maquillage > Yeux > Mascaras",
            "Beauté et Parfum > Maquillage > Yeux > Traitements et soins des cils",*/
            //"Beauté et Parfum > Parfums",
            //"Beauté et Parfum > Parfums > Coffrets de parfums",
            //"Beauté et Parfum > Parfums > Coffrets de parfums > Femme",
            //"Beauté et Parfum > Parfums > Coffrets de parfums > Homme",
            //"Beauté et Parfum > Parfums > Enfant",
            //"Beauté et Parfum > Parfums > Femme",
            //"Beauté et Parfum > Parfums > Femme > Brume corporelle parfumée",
            //"Beauté et Parfum > Parfums > Femme > Coffrets de parfums",
            //"Beauté et Parfum > Parfums > Femme > Eau de cologne",
            //"Beauté et Parfum > Parfums > Femme > Eau de parfum",
            //"Beauté et Parfum > Parfums > Femme > Eau de toilette",
            //"Beauté et Parfum > Parfums > Femme > Extrait de Parfum",
            //"Beauté et Parfum > Parfums > Femme > Huiles de parfum",
            //"Beauté et Parfum > Parfums > Homme",
            //"Beauté et Parfum > Parfums > Homme > Après-rasage",
            //"Beauté et Parfum > Parfums > Homme > Coffrets de parfums",
            //"Beauté et Parfum > Parfums > Homme > Eau de cologne",
            //"Beauté et Parfum > Parfums > Homme > Eau de parfum",
            //"Beauté et Parfum > Parfums > Homme > Eau de toilette",
            //"Beauté et Parfum > Parfums > Homme > Extrait de Parfum",
            //"Beauté et Parfum > Parfums > Vaporisateurs de parfum",
           /* "Beauté et Parfum > Rasage et Épilation",
            "Beauté et Parfum > Rasage et Épilation > Accessoires",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Accessoires et pièces détachées pour épilateurs",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Blaireaux",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Bols pour savon à barbe",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Cartouches de rechange pour épilateurs à lumière pulsée",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Chargeurs et alimentation",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Chauffe-cires",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Grilles de rechange",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Huiles pour tondeuses",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Lames de rasage femme",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Lames de rasage homme",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Nettoyage",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Nettoyage > Brossettes",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Nettoyage > Cartouches",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Nettoyage > Liquides et sprays",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Sabots pour tondeuses",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Spatules",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Supports pour rasoirs",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Têtes de rasoirs rotatives",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Étuis pour rasoirs",
            "Beauté et Parfum > Rasage et Épilation > Accessoires > Étuis pour rasoirs électriques",
            "Beauté et Parfum > Rasage et Épilation > Ciseaux",
            "Beauté et Parfum > Rasage et Épilation > Ciseaux > Ciseaux de coiffure",
            "Beauté et Parfum > Rasage et Épilation > Ciseaux > Ciseaux pour moustache et barbe",
            "Beauté et Parfum > Rasage et Épilation > Ciseaux > Ciseaux pour nez et oreilles",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Accessoires",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Accessoires > Blaireaux",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Accessoires > Bols pour savon à barbe",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Accessoires > Supports",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Accessoires > Étuis pour rasoirs",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Rasage manuel femme",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Rasage manuel femme > Lames de rasage",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Rasage manuel femme > Rasoirs manuels",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Rasage manuel homme",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Rasage manuel homme > Kits de rasage manuel",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Rasage manuel homme > Lames de rasage",
            "Beauté et Parfum > Rasage et Épilation > Rasage manuel > Rasage manuel homme > Rasoirs manuels",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Accessoires",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Accessoires > Chargeurs et alimentation",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Accessoires > Grilles de rechange",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Accessoires > Nettoyage",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Accessoires > Nettoyage > Brossettes",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Accessoires > Nettoyage > Cartouches",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Accessoires > Nettoyage > Liquides et sprays",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Accessoires > Têtes de rasoirs rotatives",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Accessoires > Étuis pour rasoirs",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Rasoirs électriques femme",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Rasoirs électriques homme",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Rasoirs électriques homme > Rasoirs à grille",
            "Beauté et Parfum > Rasage et Épilation > Rasoirs électriques et accessoires > Rasoirs électriques homme > Rasoirs à têtes rotatives",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage > Après-rasage",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage > Baumes et crèmes",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage > Gants exfoliants",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage > Gels",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage > Huiles à barbe",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage > Lotions et fluides",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage > Produits astringents",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage > Prévention et traitement des poils incarnés",
            "Beauté et Parfum > Rasage et Épilation > Soins post-épilatoires et après-rasage > Sets",
            "Beauté et Parfum > Rasage et Épilation > Soins pré-épilation et pré-rasage",
            "Beauté et Parfum > Rasage et Épilation > Soins pré-épilation et pré-rasage > Baumes et crèmes",
            "Beauté et Parfum > Rasage et Épilation > Soins pré-épilation et pré-rasage > Gants exfoliants",
            "Beauté et Parfum > Rasage et Épilation > Soins pré-épilation et pré-rasage > Gels",
            "Beauté et Parfum > Rasage et Épilation > Soins pré-épilation et pré-rasage > Huiles",
            "Beauté et Parfum > Rasage et Épilation > Soins pré-épilation et pré-rasage > Kits de soins pré-épilatoires et pré-rasage",
            "Beauté et Parfum > Rasage et Épilation > Soins pré-épilation et pré-rasage > Lotions",
            "Beauté et Parfum > Rasage et Épilation > Soins pré-épilation et pré-rasage > Mousses",
            "Beauté et Parfum > Rasage et Épilation > Soins pré-épilation et pré-rasage > Savons",
            "Beauté et Parfum > Rasage et Épilation > Tondeuses et accessoires",
            "Beauté et Parfum > Rasage et Épilation > Tondeuses et accessoires > Huiles",
            "Beauté et Parfum > Rasage et Épilation > Tondeuses et accessoires > Sabots",
            "Beauté et Parfum > Rasage et Épilation > Tondeuses et accessoires > Tondeuses corps",
            "Beauté et Parfum > Rasage et Épilation > Tondeuses et accessoires > Tondeuses femme",
            "Beauté et Parfum > Rasage et Épilation > Tondeuses et accessoires > Tondeuses multifonctionnelles et kits",
            "Beauté et Parfum > Rasage et Épilation > Tondeuses et accessoires > Tondeuses visage",
            "Beauté et Parfum > Rasage et Épilation > Tondeuses et accessoires > Tondeuses à cheveux",
            "Beauté et Parfum > Rasage et Épilation > Épilation",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Crèmes dépilatoires",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Disques d’épilation",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Décoloration",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Pinces à épiler",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilateurs et accessoires",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilateurs et accessoires > Accessoires et piéces détachées",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilateurs et accessoires > Épilateurs",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation au fil",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation laser",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à la cire",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à la cire > Bandes de cire",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à la cire > Chauffe-cires",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à la cire > Cires",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à la cire > Cires au sucre",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à la cire > Kits d'épilation à la cire",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à la cire > Poudres de talc",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à la cire > Roll-on",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à la cire > Spatules",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à lumière pulsée",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à lumière pulsée > Cartouches de rechange",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation à lumière pulsée > Épilateurs à lumière pulsée",
            "Beauté et Parfum > Rasage et Épilation > Épilation > Épilation électrolyse",
            "Beauté et Parfum > Soins pour la peau",
            "Beauté et Parfum > Soins pour la peau > Coffrets cadeaux",
            "Beauté et Parfum > Soins pour la peau > Corps",
            "Beauté et Parfum > Soins pour la peau > Corps > Amincissants et raffermissants",
            "Beauté et Parfum > Soins pour la peau > Corps > Appareils de tonification",
            "Beauté et Parfum > Soins pour la peau > Corps > Argile",
            "Beauté et Parfum > Soins pour la peau > Corps > Autobronzants",
            "Beauté et Parfum > Soins pour la peau > Corps > Crème solaire",
            "Beauté et Parfum > Soins pour la peau > Corps > Gommages",
            "Beauté et Parfum > Soins pour la peau > Corps > Poudre de talc",
            "Beauté et Parfum > Soins pour la peau > Corps > Produits hydratants",
            "Beauté et Parfum > Soins pour la peau > Corps > Produits hydratants > Après-soleil",
            "Beauté et Parfum > Soins pour la peau > Corps > Produits hydratants > Beurres",
            "Beauté et Parfum > Soins pour la peau > Corps > Produits hydratants > Crèmes",
            "Beauté et Parfum > Soins pour la peau > Corps > Produits hydratants > Huiles",
            "Beauté et Parfum > Soins pour la peau > Corps > Produits hydratants > Lait et lotions",
            "Beauté et Parfum > Soins pour la peau > Corps > Produits hydratants > Mousses",
            "Beauté et Parfum > Soins pour la peau > Cou et décolleté",
            "Beauté et Parfum > Soins pour la peau > Lèvres",
            "Beauté et Parfum > Soins pour la peau > Lèvres > Baumes",
            "Beauté et Parfum > Soins pour la peau > Lèvres > Crème solaire",
            "Beauté et Parfum > Soins pour la peau > Lèvres > Gommages",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Accessoires pour la pédicure",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Accessoires pour la pédicure > Coupeurs et accessoires",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Accessoires pour la pédicure > Pierres ponce",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Accessoires pour la pédicure > Râpes à pieds",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Bains de paraffine",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Bains de pieds",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Chaussettes hydratantes",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Crèmes pour les mains et les ongles",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Crèmes pour les pieds",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Fluides pour éliminer les cuticules",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Gants hydratants",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Gommages",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Huiles pour cuticules",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Soins pour les cuticules",
            "Beauté et Parfum > Soins pour la peau > Mains et pieds > Traitements des odeurs de pieds",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Activateurs et accélérateurs de bronzage",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Après-soleil",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Autobronzants",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Autobronzants > Corps",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Autobronzants > Visage",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Protection solaire",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Protection solaire > Corps",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Protection solaire > Lèvres",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Protection solaire > Visage",
            "Beauté et Parfum > Soins pour la peau > Solaires et bronzants > Solariums et lampes UV",
            "Beauté et Parfum > Soins pour la peau > Visage",
            "Beauté et Parfum > Soins pour la peau > Visage > Appareils de tonification",
            "Beauté et Parfum > Soins pour la peau > Visage > Autobronzants",
            "Beauté et Parfum > Soins pour la peau > Visage > Crème solaire",
            "Beauté et Parfum > Soins pour la peau > Visage > Hydratants",
            "Beauté et Parfum > Soins pour la peau > Visage > Hydratants > BB et CC crèmes",
            "Beauté et Parfum > Soins pour la peau > Visage > Hydratants > Soin de jour",
            "Beauté et Parfum > Soins pour la peau > Visage > Hydratants > Soin de nuit",
            "Beauté et Parfum > Soins pour la peau > Visage > Hydratants > Sprays et brumes hydratantes",
            "Beauté et Parfum > Soins pour la peau > Visage > Lotions toniques",
            "Beauté et Parfum > Soins pour la peau > Visage > Masques",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Appareils de nettoyage et brosses",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Crèmes et laits",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Disques, cotons et cotons-tiges pour le visage",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Démaquillants",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Eponges et lingettes démaquillantes",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Exfoliants",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Gommages",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Patchs",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Peaux à problèmes",
            "Beauté et Parfum > Soins pour la peau > Visage > Nettoyants et exfoliants > Savons",
            "Beauté et Parfum > Soins pour la peau > Yeux",
            "Beauté et Parfum > Soins pour la peau > Yeux > Crèmes contour des yeux",
            "Beauté et Parfum > Soins pour la peau > Yeux > Démaquillants",
            "Beauté et Parfum > Soins pour la peau > Yeux > Gels contours des yeux",
            "Beauté et Parfum > Soins pour la peau > Yeux > Masques de refroidissement",
            "Beauté et Parfum > Soins pour la peau > Yeux > Patchs",
            "Beauté et Parfum > Soins pour la peau > Yeux > Sérums et fluides contour des yeux",
            "Beauté et Parfum > Vernis à ongles et manucure",
            "Beauté et Parfum > Vernis à ongles et manucure > Bases de vernis",
            "Beauté et Parfum > Vernis à ongles et manucure > Dissolvants",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Accessoires de décoration pour nail art",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Accessoires de décoration pour nail art > Autocollants et pochoirs",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Accessoires de décoration pour nail art > Bijoux",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Accessoires de décoration pour nail art > Fimo",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Accessoires de décoration pour nail art > Kits de nail art",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Accessoires de décoration pour nail art > Paillettes et micro-billes",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Accessoires de décoration pour nail art > Patchs",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Accessoires de décoration pour nail art > Strass",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Bases de vernis",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Dissolvants",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Faux ongles et accessoires",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Faux ongles et accessoires > Chablons et formes",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Faux ongles et accessoires > Colles pour ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Faux ongles et accessoires > Faux ongles et capsules",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Faux ongles et accessoires > Fibres de verre et de soie",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Faux ongles et accessoires > Gels et résines",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Faux ongles et accessoires > Poudres et liquides acryliques",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Kits de manucure",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Brosses à ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Correcteurs de vernis",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Coussins repose-main",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Mains et doigts d'entraînement",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Modèles de présentation",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Pantoufles de spa",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Stylos et pinceaux de nail art",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Séchoirs à ongles et lampes UV",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Séparateurs d'orteils",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Tampons et raclettes",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Outils > Trousses d'accessoires de manucure",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Top coats",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Vernis gels semi-permanents",
            "Beauté et Parfum > Vernis à ongles et manucure > Décorations et accessoires > Vernis à ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Accessoires pour nail art",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Accessoires pour nail art > Autocollants et pochoirs",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Accessoires pour nail art > Bijoux",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Accessoires pour nail art > Fimo",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Accessoires pour nail art > Kits de nail art",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Accessoires pour nail art > Paillettes et micro-billes",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Accessoires pour nail art > Patchs",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Accessoires pour nail art > Strass",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Ciseaux à ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Coupe-cuticules",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Coupe-ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Kits d'outils de manucure et pédicure",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Limes à ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Manucure éléctrique",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Brosses à ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Correcteurs de vernis",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Coussins repose-main",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Mains et doigts d'entraînement",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Modèles de présentation",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Pantoufles de spa",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Stylos et pinceaux de nail art",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Séchoirs à ongles et lampes UV",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Séparateurs d'orteils",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Tampons et raclettes",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Outils pour vernis > Trousses d'accessoires de manucure",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Pierres ponce",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Pinces guillotine pour faux ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Pinces à ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Polissoirs",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Pousse-cuticules",
            "Beauté et Parfum > Vernis à ongles et manucure > Outils, coupe et polissage > Râpes à pieds",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les mains et pieds",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les mains et pieds > Bains de paraffine",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les mains et pieds > Bains de pied",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les mains et pieds > Chaussettes hydratantes",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les mains et pieds > Crèmes pour mains et ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les mains et pieds > Crèmes pour pieds",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les mains et pieds > Gants hydratants",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les mains et pieds > Gommages",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les ongles",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les ongles > Blanchissants",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les ongles > Durcisseurs",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les ongles > Lissants et anti-stries",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les ongles > Produits de croissance",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les ongles > Réparateurs et nourrissants",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les ongles > Soin des cuticules",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les ongles > Soin des cuticules > Huiles pour cuticules",
            "Beauté et Parfum > Vernis à ongles et manucure > Soins pour les ongles > Soin des cuticules > Soins de cuticules et crèmes de réparation",
            "Beauté et Parfum > Vernis à ongles et manucure > Top coats",
            "Beauté et Parfum > Vernis à ongles et manucure > Vernis gels semi-permanents",
        "Beauté et Parfum > Vernis à ongles et manucure > Vernis à ongles"*/],
        "es": [],
        "com": [
            // "Health & Household > Vitamins & Dietary Supplements",
            // "sports & outdoors > outdoor recreation > camping & hiking",
            "sports & outdoors > sports & fitness > other sports > martial arts > weapons > ninja weapons"],

    }
    const prohibitedProducts = {
        "fr": [
            "Huile de Coco",
            "Masseur femme",
            "Lesbian",
            "licking",
            "Wand Massage",
            "Gay",
            "Sucker",
            "Hélicoptère",
            "Œuf Vibrant",
            "Masseur de Silicone",
            "Masseur Portable",
            "Planeur",
            "Avion",
            "masque tactile",
            "masque facial tactile",
            "Balaclave",
            "face shield",
            "cagoule",
            "hemp",
            "Outillage d'armurerie",
            "Fumigène",
            "Grenade",
            "Tonfa",
            "détonateur",
            "masturbation",
            "Liliya&& godes",
            "Vibromasseurs",
            "Sex Toy",
            "Gode",
            "Penis",
            "dildo",
            "Armes complètes",
            "Armes de ninja",
            "Armes complètes",
            "Bokken",
            // "Kamas",
            "Tonfas",
            "couteaux à lame fixe",
            "Couteaux pliants",
            "Haches de combat",
            "Pistolets",
            "Pistolet",

            "Knife",
            "Tronçonneuse",
            "chalumeau",
            "briquet",
            // "gaz",
            "code d'activation",

            "abonnement",
            "Live Cards",
            "aérosol",
            "aerosol",
            "butane",
            "propane",
            "spy",
            "espion",
            "drone",
            "Couteau pliant",
            "Couteau de Poche Pliant",
            "Daggers",
            "Gut Lore",
            "Bâton de Policier",
            "Folding Knife",
            "katana",
            "Couteau d'extérieur",
            "KNIFY",
            "KARAMBIT Lore",
            "couteaux crochet",
            "Epée",
            "armes de ninja",
            "Kit ninja",
            "Couteau Suisse",
            "Regulus Knife",
            "FARDEER KNIFE",
            "Couteau de Chasse",
            "Couteau papillon",
            "Couteau de Survie",
            "matraque",
            "coup de poing américain",
            "bombe lacrymogène",
            "Spray de défense"


        ],

        "com": ["knife", "explosive", "Massage Woman",
            "Lesbian",
            "Licking",
            "Wand Massage",
            "Gay",
            "Sucker",
            "Helicopter",
            "Hovering",
            // "Plane",

            "Balaclave",
            "face shield",

            "Hemp",
            "Weapon",
            "Pepper Spray",
            "Grenade",
            "Tonfa",
            "detonator",
            "masturbation",
            "Liliya && dildos",
            "Vibrators",
            "Sex Toy",
            "Dildo",
            "Penis",
            "Dildo",

            "Bokken",
            // "Kamas",
            "TonfasWe",
            // "Blade",

            "axis of fight",
            "Pistol",
            "coconut oil",
            "Blowtorch",


            "subscription",
            "Live Cards",

            "aerosol",
            "butane",
            "propane",
            "Spy",

            "drone",
            "Folding knife",
            "Folding Pocket Knife",
            "Dagger",
            "Gut Lore",
            "Police Staff",
            "Folding Knife",
            "Katana",
            "Outdoor knife",
            "KNIFY",
            "KARAMBIT Lore",
            "hook knives",
            "Sword",
            "Airsoft",
            "Ninja Kit",
            "Ninja stars",
            "ninja",
            "Ninja",

            "throwing axe",
            "hatchet",
            "tomahawk",
            "hammer",
            "machete",
            "Machete",

            "Swiss knife",
            "Regulus Knife",
            "FARDEER KNIFE",

            "baton",
            "pepper spray",
            "Spray of defense"
        ],
    }
    const allowedCategories = {
        "fr": ["Hygiène et Santé > Cigarettes électroniques, chichas et accessoires > Chichas électroniques et accessoires > E-liquides et recharges",
            "Hygiène et Santé > Cigarettes électroniques, chichas et accessoires > Chichas électroniques et accessoires > Chichas électroniques",
            "Hygiène et Santé > Cigarettes électroniques, chichas et accessoires > Chichas électroniques et accessoires > Accessoires",
            "Hygiène et Santé > Cigarettes électroniques, chichas et accessoires > Cigarettes électroniques et accessoires > Accessoires",
            "Hygiène et Santé > Cigarettes électroniques, chichas et accessoires > Cigarettes électroniques et accessoires > Cigarettes électroniques",
            "Hygiène et Santé > Cigarettes électroniques, chichas et accessoires > Cigarettes électroniques et accessoires > E-liquides et recharges",
            "Hygiène et Santé > Cigarettes électroniques, chichas et accessoires > Cigarettes électroniques et accessoires > E-liquides et recharges",



        ]
        , "com": [],
    }
    const precommandeDetection = {
        "uk": "Pre-order This Item Today",
        "fr": "Précommander",
        "es": "",
        "com": "",
    }
    const alcoholDetection = {
        "uk": "Alcohol is not for sale to people under the age of 18",
        "fr": "Interdiction de vente de boissons alcooliques aux mineurs de moins de 18 ans",
        "es": "",
        "com": "Alcohol is not for sale to people under the age of 18"
    }
    const prohibitedGlobal = {
        "uk": "",
        "fr": "moins de 18 ans",
        "es": "",
        "com": "under the age of 18",
    }
    const getDomain = () => {
        // amazon.fr
        return ".amazon.fr"
    }
    const getAllowedCategories = () => {
        let key = getCountryFromAmazonProductPageUrl()
        return allowedCategories[key]
    }
    const getProhibitedCategories = () => {
        let key = getCountryFromAmazonProductPageUrl()
        return prohibitedcategories[key]
    }
    const getProhibitedProducts = () => {
        let key = getCountryFromAmazonProductPageUrl()
        return prohibitedProducts[key]
    }
    const getAmazonKey = () => {
        return "amazon" + getCountryFromAmazonProductPageUrl()
    }
    const getCountryFromAmazonProductPageUrl = () => {
        var re = /amazon(\.co)*\.(uk|fr|de|it|es|com)/
        var match = re.exec(document.location.href)
        if (document.location.href.match(re)) {
            return match[2]
        }
        else {
            return null
        }
    }
    const isCartPage = () => {
        return document.location.href.indexOf('/cart/') !== -1
    }
    const isProductPage = () => {
        return document.location.href.indexOf('/gp/product/') !== -1 || document.location.href.indexOf('/dp/') !== -1
    }
    const goToCart = () => {
        console.log("navigation")
        window.location.href = document.location.origin + "/gp/cart/view.html/"
        return;
    }
    const isAddedItemPage = () => {
        return document.location.href.indexOf('/gp/huc/') !== -1
    }
    const toFloat = (str) => {

        let pattern = new RegExp("([\\d\\.]+)")
        // console.log(str)
        str = str.replace(',', '.')
        let price = parseFloat(str.match(pattern)[1]).toFixed(2) * 100
        return Math.round(price) / 100
    }
    const hasFlash = (doc = document) => {

        if ($(doc).find('[id^=deal_expiry_timer]').length || $(doc).find("span.gb-accordion-active", "#LDBuybox").text().trim().indexOf("Vente Flash") != -1) return true
        return false
    }
    const getFlashTime = (doc = document) => {
        if ($(doc).find('[id^=deal_expiry_timer]').length) {
            if (document.location.href.indexOf("amazon.com")) {
                return $(doc).find('[id^=deal_expiry_timer]').text().split('in ')[1]
            } else { return $(doc).find('[id^=deal_expiry_timer]').text().split('dans ')[1] }
        }
        return "0"
    }
    const bookLivraison = (doc = document) => {
        try {
            if ($(doc).find("#buyNewInner").text().trim().indexOf("Livraison à partir de EUR 0,01") != -1) return true
            return false
        } catch (error) {
            return false
        }

    }
    const getFlashPrice = (doc = document) => {
        return $(doc).find("#priceblock_dealprice").text().trim()
    }
    const priceFormat = (pricePage) => {
        let pattern, currency, price;
        if (document.location.href.indexOf('amazon.fr') !== -1 || document.location.href.indexOf('amazon.de') !== -1) {
            currency = "EUR"
            // console.log(pricePage, isNaN(pricePage))
            let priceType = isNaN(pricePage)
            // pricePage = '' + pricePage
            // console.log(pricePage, isNaN(pricePage))


            // console.log(pricePage)
            if (priceType == true) {
                pricePage = pricePage.replace('&nbsp;', '')//.replace(' ', '')
                pattern = new RegExp("([\\d\\.]+)");
                pricePage = pricePage.replace(',', '.')
                price = parseFloat(pricePage.match(pattern)[1]).toFixed(2) * 100
            } else {
                // pattern = new RegExp("([\\d\\.]+)");
                price = parseFloat(pricePage).toFixed(2) * 100
            }
            // console.log(pricePage.match(pattern))

            price = Math.round(price)
        }
        if (document.location.href.indexOf('amazon.co.uk') !== -1) {
            currency = "\u00A3"
            pattern = new RegExp("\u00A3([\\d\\.]+)");
            price = parseFloat(pricePage.match(pattern)[1]).toFixed(2) * 100
            price = Math.round(price)

        }
        if (document.location.href.indexOf('amazon.com') !== -1) {
            // console.log(pricePage)

            // currency = "\u0024";
            // currency = "\u00A3";
            // currency = "€"
            currency = "USD"


            pricePage = pricePage.replace(',', '')
            // console.log(pricePage)
            pattern = new RegExp("([\\d\\.]+)");
            // console.log(pricePage.split('$')[1].match(pattern))

            price = parseFloat(pricePage.match(pattern)[1]).toFixed(2) * 100
            price = Math.round(price)

        }
        return { price: price, currency: currency }
    }
    const checkProhibition = (doc = document) => {
        // allow categories 
        // console.log('checking prohib')
        // console.log($(doc).find('#productTitle').text().trim())
        let prMetal = [...doc.querySelectorAll('.prodDetSectionEntry')].map(r => {
            return {
                key: r.textContent.trim(),
                value: (r.nextElementSibling?r.nextElementSibling.textContent.trim():""),
            }
        }).filter(el => el["key"].indexOf("Metal") != -1)
        //    console.log()
        if (prMetal.length > 0) {
            let prohib = prMetal.some(el => {
                return el["value"].trim().toLowerCase().indexOf("yellow gold") != -1 ||
                    el["value"].trim().toLowerCase().indexOf("silver") != -1
                    || el["value"].trim().toLowerCase().indexOf("white gold") != -1
                    //   || el["value"].trim().toLowerCase().indexOf("or jaune") != -1
                    || el["value"].trim().toLowerCase().indexOf("platinium") != -1
                    //   || el["value"].trim().toLowerCase().indexOf("or_jaune") != -1
                    || el["value"].trim().toLowerCase().indexOf("yellow-gold") != -1
            })
            // console.log(prohib)
            if (!!prohib) return true
        }
        // precious metals 
        // if (!!$(doc).find('#technicalSpecifications_feature_div .a-row .a-column table tbody tr')) {
        //     let preciousMetal = false
        //     // console.log("metal")
        //     $(doc).find('#technicalSpecifications_feature_div .a-row .a-column table tbody tr').each((idx, elem) => {
        //         // console.log($(elem).find('td.a-span7').text().trim(), elem["value"])
        //         let el = {
        //             key : $(elem).find('th.a-span5').text(),
        //             value:$(elem).find('td.a-span7').text()
        //         }
        //         if (
        //             (
        //                 $(elem).find('th.a-span5').text().indexOf("Métal") != -1
        //                 || $(elem).find('th.a-span5').text().indexOf("Poinçon métal") != -1
        //             )
        //             &&
        //             (
        //                 el["value"].trim().toLowerCase().indexOf("argent") != -1
        //                 || el["value"].trim().toLowerCase().indexOf("or ") != -1
        //                 || el["value"].trim().toLowerCase().indexOf("or jaune") != -1
        //                 || el["value"].trim().toLowerCase().indexOf("platine") != -1
        //                 || el["value"].trim().toLowerCase().indexOf("or_jaune") != -1
        //                 || el["value"].trim().toLowerCase().indexOf("yellow-gold") != -1
        //             )
        //         ) {
        //             preciousMetal = true
        //         }
        //     })
        //     // console.log("precious metal", preciousMetal)
        //     if (!!preciousMetal) return preciousMetal
        // }
        if (!!doc.querySelector('#bylineInfo') && doc.querySelector('#bylineInfo').innerText.indexOf('London General Insurance Company Limited') != -1) return true
        //Allstate Protection Plan
        if ($(doc).find('#productTitle').text().indexOf('Allstate Protection Plan') != -1 || $(doc).find('#productTitle').text().indexOf('Assurant Protection') != -1 ||$(doc).find('#productTitle').text().indexOf('assurance panne') != -1 || $(doc).find('#productTitle').text().indexOf('Assurant Protection') != -1 || $(doc).find('#productTitle').text().indexOf('assurance dommage') != -1|| $(doc).find('#productTitle').text().indexOf('Amazon Protect') != -1 || $(doc).find('#productTitle').text().indexOf('3-Year EXT') != -1) {
            // alert("assurance")
            // 
            // London General Insurance Company Limited 
            return true;
        }

        // console.log(document.querySelector("input#add-to-cart-button"))
        // console.log($(doc).find('input#add-to-cart-button[value]').attr('value'))
        // if (!!$(doc).find('#add-to-cart-button').attr("value")) {
        //     if ($(doc).find('#add-to-cart-button').attr("value").indexOf(precommandeDetection[getCountryFromAmazonProductPageUrl()]) != -1) {
        //         console.log("precommande detected")

        //         return true
        //     }
        // }

        // console.log($(doc).find('#ageWarning_feature_div').text())
        if (!!doc.querySelector('#ageWarning_feature_div')) {
            if ($('#ageWarning_feature_div div div span').text().indexOf(alcoholDetection[getCountryFromAmazonProductPageUrl()]) != -1) {
                // console.log("alcohol detected")
                return true
            }
        }

        if ($(doc).find('body').text().indexOf(alcoholDetection[getCountryFromAmazonProductPageUrl()]) != -1) {
            // console.log("alcohol ???")
            return true;
        }

        let cats = []
        let dutycategory
        if (!!doc.querySelector('#wayfinding-breadcrumbs_feature_div')) {
            cats = [
                $(doc).find('#wayfinding-breadcrumbs_feature_div ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray().join(' > '),
                ...$(doc).find('.zg_hrsr_ladder').map((v, u) => {
                    return $(u).find('a').map((r, v) => $(v).text().trim()).toArray().join(' > ')//.split('dans ').join('')
                }).toArray()
            ].filter(function (el) {
                return el.trim() != null;
            });
            dutycategory = $(doc).find('#wayfinding-breadcrumbs_feature_div ul li span').map((idx, item) => $(item).text().trim()).toArray().join('')

        } else {
            cats = [
                $(doc).find('#wayfinding-breadcrumbs_container ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray().join(' > '),
                // ...$(doc).find('.zg_hrsr_ladder').map((v, u) => {
                //     return $(u).find('a').map((r, v) => $(v).text().trim()).toArray().join(' > ')//.split('dans ').join('')
                // }).toArray()
            ].filter(function (el) {
                return el.trim() != null;
            });
            dutycategory = $(doc).find('#wayfinding-breadcrumbs_container ul li span').map((idx, item) => $(item).text().trim()).toArray().join('')

        }

        if (document.location.href.indexOf('amazon.fr') != -1) {
            cats = [[...new Set(cats.map(t => t.trim().toLowerCase().replace(/&/g, 'et')))].join('').replace(/›/g, '>')]
        } else {
            cats = [[...new Set(cats.map(t => t.trim().toLowerCase()))].join('').replace(/›/g, '>')]

        }

        // let cats = [
        //     $(doc).find('#wayfinding-breadcrumbs_feature_div ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray().join(' > '),
        //     ...$(doc).find('.zg_hrsr_ladder').map((v, u) => {
        //         return $(u).find('a').map((r, v) => $(v).text().trim()).toArray().join(' > ')//.split('dans ').join('')
        //     }).toArray()
        // ]
        // let dutycategory = $(doc).find('#wayfinding-breadcrumbs_feature_div ul li span').map((idx, item) => $(item).text().trim()).toArray().join('')
        // let catsAllow = getAllowedCategories().includes(dutycategory.replace(/›/g, '>'))
        // let catsAllowLong = cats.some(el => {
        //     // console.log(el)
        //     return getAllowedCategories().map(r => r.replace(/›/g, '>')/*.split('>').join(' > ')*/).includes(el.trim().replace(/›/g, '>'))
        // })
        // if (!catsAllow || !catsAllowLong) return false

        // if ($(doc).find('body').text().indexOf(prohibitedGlobal[getCountryFromAmazonProductPageUrl()]) != -1) {
        //     // console.log("global")
        //     return true;
        // }

        // console.log(catsAllowLong,)


        // console.log("after precious metal")

        return false

    }
    const getVatNumber = async (sellerLink) => {
        let data = await fetch(sellerLink, { credentials: "include", mode: "no-cors" })
        data = await data.text()
        let doc = new DOMParser().parseFromString(data, 'text/html')
        let title = doc.querySelector('#-component-heading')
        if (title.innerText.indexOf('Informations vendeur détaillées') != -1) {
            let list = title.parentNode.querySelectorAll('ul li')
            return {
                hasVAT: true,
                VATnum: [...list].filter(i => i.innerText.indexOf('Numéro TVA:') != -1)[0].innerText.split(':')[1]
            }
        } else
            return {
                hasVAT: false,
                VATnum: ''
            }
    }
    const getDimensions = (measures) => {
        // console.log(measures)
        // console.log(measures.length)
        try {


            return measures.map(m => {
                // console.log(m)
                if (m.indexOf("-") != -1) m = m.split('-')[1].trim()
                if (m.indexOf('centimètres') != -1 || m.indexOf('cm') != -1) return toFloat(m.trim().replace(' cm', '').replace(' mm', '').replace(' centimètres', '').replace(' millimètres', '').replace(' mètres', '').trim())
                if (m.indexOf('mm') != -1) return toFloat(m.trim().replace(' cm', '').replace(' mm', '').replace(' centimètres', '').replace(' millimètres', '').replace(' mètres', '').trim()) / 10
                if (m.indexOf('millimètres') != -1) return toFloat(m.trim().replace(' cm', '').replace(' mm', '').replace(' centimètres', '').replace(' millimètres', '').replace(' mètres', '').trim()) / 10
                
                if (m.indexOf('mètres') != -1) return toFloat(m.trim().replace(' cm', '').replace(' mm', '').replace(' centimètres', '').replace(' millimètres', '').replace(' mètres', '').trim()) * 100
                let coeff = 1
                if (measures[2].indexOf("mm") != -1 || measures[2].indexOf("millimètres") != -1) coeff = 0.1
                
                if (measures[2].indexOf("mètres") != -1&& !(measures[2].indexOf('millimètres') != -1) && !(measures[2].indexOf('centimètres') != -1)) coeff = 10

                return toFloat(m.trim().replace(' millimètres', '').replace(' cm', '').replace(' mm', '').replace(' centimètres', '').replace(' mètres', '').trim()) * coeff

            })
        } catch (error) {
            return [0, 0, 0]
        }
        // dimensions.length = measures.split('x')[0].trim()
        // dimensions.width = measures.split('x')[1].trim()
        // dimensions.height = measures.split('x')[2].replace(' cm', '').replace(' mm', '').replace(' centimètres', '').trim()
        // volumicWeight = 
        // toFloat(dimensions.length) * sCoef * toFloat(dimensions.height) * sCoef * toFloat(dimensions.width) * sCoef / 5000
        // volumicWeight = Math.max(volumicWeight, applicableWeight)
    }

    const checkExtraTaxe = (doc = document) => {
        let taxe = false
        if ($(doc).find('#feature-bullets').text().trim().indexOf('PEFC') != -1 || $(doc).find('#feature-bullets').text().trim().indexOf('FSC100') != -1 || $(doc).find('#productDescription').text().trim().indexOf('FSC 100') != -1) taxe = (taxe == true ? taxe : true)
        if ($(doc).find('#productDescription').text().trim().indexOf('PEFC') != -1 || $(doc).find('#productDescription').text().trim().indexOf('FSC100') != -1 || $(doc).find('#productDescription').text().trim().indexOf('FSC 100') != -1) taxe = (taxe == true ? taxe : true)
        if ($(doc).find('#HLCXComparisonTable').length) {
            // console.log("heeey")
            $(doc).find("#HLCXComparisonTable tbody tr").each((idx, li) => {
                // console.log(($(li).find("td.comparison_baseitem_column").text().trim(),$(li).find("td.comparison_baseitem_column").text().trim().toLowerCase().indexOf("bois")!=-1))
                if ($(li).text().indexOf('Matériau') != -1) taxe = (taxe == true ? taxe : ($(li).find("td.comparison_baseitem_column").text().trim().toLowerCase().indexOf("bois") != -1))
            })
        }
        if ($(doc).find("#prodDetails").length) {
            // console.log("Cas 3 weight")
            $(doc).find(" tbody tr").each((idx, li) => {
                // console.log($(li).find('td:nth-child(2)').text().trim(),$(li).find('td:nth-child(2)').text().trim().toLowerCase().indexOf("bois")!=-1)
                contentLi = $(li).find('td:nth-child(1)').text().trim().toLowerCase()
                if (contentLi.indexOf('matériau') !== -1 || contentLi.indexOf('matière principale') !== -1 || contentLi.indexOf('composition') !== -1) taxe = (taxe == true ? taxe : ($(li).find('td:nth-child(2)').text().trim().toLowerCase().indexOf("bois") != -1))
            })


        }
        // console.log("extra",taxe)
        return taxe
    }
    const getFreeShippingConditions = (doc = document) => {

    }
    const isPrime = (doc = document) => {
        if(!!doc.querySelector('#buybox-tabular')&&doc.querySelector('#buybox-tabular').innerText.indexOf("Amazon")!=-1)
        return true;
        if (doc.querySelector('#merchant-info') && doc.querySelector('#merchant-info').innerText.indexOf('Amazon') != -1)
            return true;
        if (!![...doc.querySelectorAll('span > span.a-truncate-full.a-offscreen')].length)
            return [...doc.querySelectorAll('span > span.a-truncate-full.a-offscreen')].some(r => r.innerText.indexOf('Amazon') != -1)
        return $(doc).find("#bbop_feature_div").text().indexOf("Acquisition_AddToCart_PrimeBasicFreeTrialUpsellEligible") != -1 || $(doc).find("#merchant-info").text().indexOf("Amazon") != -1
    }
    const isBook = (categ, doc = document) => {
        isBookOk = false;
        if (isBookOk == false && doc && !!doc.querySelectorAll('.prodDetSectionEntry')) {
            let entries = [...doc.querySelectorAll('.prodDetSectionEntry')].map((r) => r.innerText);
            isBookOk = entries.some((it) => {
                if (it.toLowerCase().trim().indexOf('isbn') != -1)
                    return true;
                if (it.toLowerCase().trim().indexOf('livre') != -1)
                    return true;

            }
            );
        }
        return isBookOk || categ.some(el => el.split('>')[0].toLowerCase().indexOf("livres") != -1)
    }
    const isSoldByAmazon = (doc = document) => {
        if (doc.querySelector("#merchant-info")) return doc.querySelector("#merchant-info").innerText.indexOf('Amazon') != -1
        
        if (doc.querySelector('#bylineInfo')) return doc.querySelector('#bylineInfo').innerText.indexOf('Amazon') != -1
        return false
    }
    const isPanierPlus = (doc = document) => {
        if (doc.querySelector('.a-box-group')) return doc.querySelector('.a-box-group').innerText.indexOf('Article Panier Plus') != -1
        return false
    }
    const getDimensionsUs = (doc = document) => {
        let info = {};
        // console.log("USCALLED")
        let arr3 = [...doc.querySelectorAll('td.bucket .content ul li')].map(r => {
            if (!r.querySelector('b'))
                return;
            let key = (r.querySelector('b') ? r.querySelector('b').innerText.split(':')[0].trim() : "")
            let value = (r.innerText ? r.innerText.split(key)[1].trim() : "")
            // 	r.innerText.trim()
            return {
                key: key,
                value: value

            }

        }
        ).filter(r => !!r)
        info.hasProdDim = false;
        info.hasProdShipWeight = false;
        if (!!doc.querySelectorAll('.prodDetSectionEntry')) {
            // console.log("here",doc.querySelector('title').innerText);
            [...arr3, ...doc.querySelectorAll('.prodDetSectionEntry')].forEach(r => {
                let key = (r.hasOwnProperty('key') ? r["key"].trim() : r.innerText.trim())
                let val = (r.hasOwnProperty('value') ? r["value"].trim() : r.parentElement.querySelector('td').innerText.trim())
                if (val.indexOf('(') != -1) val = val.split('(')[0].trim()
                if (val.indexOf(':') != -1) val = val.split(':')[1].trim()
                switch (key) {
                    case 'Package Weight':
                        if (info.hasOwnProperty('shippingWeight'))
                            break;
                        info.shippingWeight = val
                        break;
                    case 'Shipping Weight':
                        info.hasProdShipWeight = true;
                        info.shippingWeight = val
                        break;
                    case 'Item Weight':
                        if (info.hasOwnProperty('shippingWeight'))
                            break;
                        info.shippingWeight = val
                        break;
                    case 'Item Dimensions L x W x H':
                        if (info.hasOwnProperty('productDimensions'))
                            break;
                        info.productDimensions = val
                        break;
                    case 'Product Dimensions':
                        info.hasProdDim = true;
                        info.productDimensions = val
                        break;
                    default:
                        break;
                }


            }
            )
            // console.log(info)
        } 
        if (!!doc.getElementById('tech-specs-table-right')) {
            [...doc.querySelectorAll('#tech-specs-table-right tbody tr td p')].forEach(el => {
                if (el.innerText.indexOf('Height') != -1) info.height = el.innerText.trim()
                if (el.innerText.indexOf('Weight') != -1) info.weight = el.innerText.trim()
                if (el.innerText.indexOf('Width') != -1) info.width = el.innerText.trim()
                if (el.innerText.indexOf('Depth') != -1) info.length = el.innerText.trim()

            })
        } 
        if (!!doc.getElementById('detailBullets_feature_div')) {
                    // console.log("bullets");
            [...doc.querySelectorAll('#detailBullets_feature_div ul li span.a-list-item')].forEach(r => {

                // console.log(r.innerText.split(':')[0].trim())
                switch (r.innerText.split(':')[0].trim()) {
                    case 'Package Weight':
                        if (info.hasOwnProperty('shippingWeight'))
                            break;
                        info.shippingWeight = r.parentElement.querySelector('span.a-list-item span:not(.a-text-bold)').innerText.split('(')[0].trim()
                        break;
                    case 'Shipping Weight':
                        info.shippingWeight = r.parentElement.querySelector('span.a-list-item span:not(.a-text-bold)').innerText.split('(')[0].trim()
                        break;
                    case 'Item Weight':
                        if (info.hasOwnProperty('shippingWeight'))
                            break;
                        info.shippingWeight = r.parentElement.querySelector('span.a-list-item span:not(.a-text-bold)').innerText.split('(')[0].trim()
                        break;
                    case 'Package Dimensions':

                        info.productDimensions = r.parentElement.querySelector('span.a-list-item span:not(.a-text-bold)').innerText.trim()
                        break;

                    case 'Item Dimensions L x W x H':
                        if (info.hasOwnProperty('productDimensions'))
                            break;
                        info.productDimensions = r.parentElement.querySelector('span.a-list-item span:not(.a-text-bold)').innerText.trim()
                        break;
                    case 'Product Dimensions':
                        if (info.hasOwnProperty('productDimensions'))
                            break;
                        info.productDimensions = r.parentElement.querySelector('span.a-list-item span:not(.a-text-bold)').innerText.trim()
                        break;
                    default:
                        break;
                }

            })
        }
        let measures = {}
        // console.log(info)
        measures.volumicWeight = 500
        if (info.hasOwnProperty('shippingWeight')) {
            //              console.log(info.shippingWeight)
            let coef = 1
            if (info["shippingWeight"].toLowerCase().indexOf('ounces') != -1) {
                coef = 28.35
                let weight = info["shippingWeight"].toLowerCase().split('ounces')[0].trim()
                measures.weight = Number(weight) * coef
            }
            if (info["shippingWeight"].toLowerCase().indexOf('pounds') != -1) {
                coef = 453.6
                let weight = info["shippingWeight"].toLowerCase().split('pounds')[0].trim()
                measures.weight = Number(weight) * coef
            }
            measures.ogWeight = info["shippingWeight"]
            measures.volumicWeight = (measures.weight > 500 ? measures.weight : measures.volumicWeight)
        }


        if (info.hasOwnProperty('productDimensions')) {
            // console.log(info["productDimensions"])
            let coef = 1
            if (info["productDimensions"].toLowerCase().indexOf('inches') != -1) {
                coef = 2.54
                let dims = info["productDimensions"].toLowerCase().split('inches')[0].trim()
                dims.split('x').forEach((v, i, arr) => {

                    measures.length = Number(arr[0]) * coef
                    measures.width = Number(arr[1]) * coef
                    measures.height = Number(arr[2]) * coef


                })
            }
            if (info["productDimensions"].toLowerCase().indexOf(';') != -1) {

                let weightDim = info["productDimensions"].toLowerCase().split(';')[1].trim()
                if (weightDim.toLowerCase().indexOf('pounds') != -1) {
                    let coef2 = 453.6
                    let wR = weightDim.toLowerCase().split('pounds')[0].trim()
                    if (typeof measures.weight === "undefined") measures.weight = Number(wR) * coef2
                    //    console.log(measures.weight)

                    measures.volumicWeight = (measures.weight > 500 ? measures.weight : measures.volumicWeight)

                }
                if (weightDim.toLowerCase().indexOf('ounces') != -1) {
                    let coef2 =  28.35
                    let wR = weightDim.toLowerCase().split('ounces')[0].trim()
                    if (typeof measures.weight === "undefined") measures.weight = Number(wR) * coef2
                    //    console.log(measures.weight)

                    measures.volumicWeight = (measures.weight > 500 ? measures.weight : measures.volumicWeight)

                }
            }
            
            measures.size = info["productDimensions"]
            let volW = measures.length * measures.height * measures.width / 5

            if (volW > 500) measures.volumicWeight = volW

        }
        // if (!info.hasProdDim && !info.hasProdShipWeight)
        //     measures.volumicWeight = measures.weight
        // console.log(measures)
        return measures
    }
    const parseAmazonProduct = (doc = document) => {
        // console.log(doc.innerHTML)

        // console.log($(doc).find('td.bucket div.content ul li').length)
        // let title = $(doc).find('title').text()
        let weight;
        let size;
        let contentLi;
        let shipping;
        let prohibited = checkProhibition(doc)
        let isFreeShippingOk = false
        // console.log("prohibiuted" + prohibited)
        let categories = [];
        // let docx = new DOMParser().parseFromString(doc, "text/html");

        let prodTitle = ''
        try { prodTitle = doc.querySelector('#productTitle').innerText.replace(/\^/g, '').trim() } catch (e) {
            // prodTitle = doc.querySelector('#title').innerText.replace(/\^/g, '').trim()
            // console.log(doc.innerText)
        }
        // console.log(prodTitle)
        // console.log($('#productTitle').html())

        if ($(doc).find('#HLCXComparisonTable').length) {
            // console.log("heeey")
            $(doc).find("#HLCXComparisonTable tbody tr").each((idx, li) => {
                // console.log($(li).text().trim())
                if ($(li).text().indexOf('Dimensions du produit') != -1 && (typeof size === 'undefined' || (!size.indexOf('x') != -1))) size = $(li).find("td.comparison_baseitem_column").text().trim()
                if ($(li).text().indexOf("Poids de l'article") != -1 && typeof weight === 'undefined') {
                    // console.log(typeof weight)
                    weight = $(li).find("td.comparison_baseitem_column").html()//.text().trim()
                }

                if ($(li).text().indexOf("Frais d'expédition") != -1 || $(li).text().indexOf("Frais de livraison") != -1) shipping = $(li).find("td.comparison_baseitem_column span").html().trim()
                // console.log(categories)
                categories = [...categories, ...$(doc).find('#wayfinding-breadcrumbs_feature_div ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray()]
            })
            // console.log(size)

        }
        if ($(doc).find('#productDetails_techSpec_section_1').length) {
            // console.log("heeey")
            $(doc).find("#productDetails_techSpec_section_1 tbody tr").each((idx, li) => {
//                   console.log($(li).text())
                if ($(li).text().indexOf('Dimensions du produit') != -1 && (typeof size === 'undefined' || (!size.indexOf('x') != -1))) size = $(li).find("td").text().trim()
                if ($(li).text().indexOf("Poids de l'article") != -1 && typeof weight === 'undefined') weight = $(li).find("td").html()//.text().trim()
                if ($(li).text().indexOf("Frais d'expédition") != -1) shipping = $(li).find("td").html().trim()
                // console.log(categories)
                categories = [...categories, ...$(doc).find('#wayfinding-breadcrumbs_feature_div ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray()]
            })
            // console.log(size)

        }
        if ($(doc).find("#tech-specs-table-left tbody tr").length) {
            // console.log("Cas 2 weight")
            // console.log(size)

            $(doc).find("#tech-specs-table-left tbody tr").each((idx, li) => {
                contentLi = $(li).find('td:nth-child(1)').text().trim().toLowerCase()

                // console.log(contentLi)
                if (contentLi.indexOf('weight') !== -1 || contentLi.indexOf('poids') !== -1 && typeof weight === 'undefined') weight = $(li).find('td:nth-child(2)').html()//.text().trim()
                if (contentLi.indexOf('dimensions') !== -1 || contentLi.indexOf('dimensions du produit') !== -1 || contentLi.indexOf('size') !== -1 || contentLi.indexOf('dimensions du colis') !== -1 && (typeof size === 'undefined' || (!size.indexOf('x') != -1))) size = $(li).find('td:nth-child(2)').text().trim()
                if (contentLi.indexOf('amazon bestsellers rank') !== -1 || contentLi.indexOf('classement des meilleures') !== -1) categories = $(doc).find('li.zg_hrsr_item span.zg_hrsr_ladder a').map((idx, item) => $(item).text()).toArray()
                // console.log(categories)

                categories = [...categories, ...$(doc).find('#wayfinding-breadcrumbs_feature_div ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray()]
            })
        }
        if ($(doc).find("#prodDetails".length)) {
            // console.log("Cas 3 weight")
            // console.log(size)

            $(doc).find("tbody tr").each((idx, li) => {
                contentLi = $(li).find('td:nth-child(1)').text().trim().toLowerCase()

                if (contentLi.indexOf('poids net du produit') !== -1 || contentLi.indexOf('weight') !== -1 || contentLi.indexOf("poids de l'article") !== -1 || contentLi.indexOf('poids du produit') !== -1 && typeof weight === 'undefined') weight = $(li).find('td:nth-child(2)').text().trim()
                if (contentLi.indexOf('dimensions') !== -1 || contentLi.indexOf('dimensions du produit') !== -1 || contentLi.indexOf('size') !== -1 || contentLi.indexOf('dimensions du colis') !== -1 && (typeof size === 'undefined' || size.indexOf('x') == -1)) size = $(li).find('td:nth-child(2)').text().trim()
                if (contentLi.indexOf('amazon bestsellers rank') !== -1 || contentLi.indexOf('classement des meilleures') !== -1) categories = $(doc).find('span.zg_hrsr_ladder a').map((idx, item) => $(item).text()).toArray()

                try {
                    let cat = $(doc).find('#SalesRank td.value').text().split(' (')[0].split('en ')[1].trim()
                    if (!categories.includes(cat)) categories.unshift(cat)
                } catch (error) {

                }
                // console.log(categories)

                categories = [...categories, ...$(doc).find('#wayfinding-breadcrumbs_feature_div ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray()]
            })
            // console.log(size)


        }
        if ($(doc).find('#detail_bullets_id').length) {

            // console.log("Cas 1 ")
            $(doc).find('td.bucket div.content ul li').each((idx, li) => {
                contentLi = $(li).text().trim().toLowerCase()

                // console.log(contentLi)
                // console.log(size)
                // console.log($(li).text().split(':')[1].trim())
                if (contentLi.indexOf('weight') !== -1 || contentLi.indexOf('poids') !== -1 && typeof weight === 'undefined') weight = $(li).text().split(':')[1].trim()//.replace(' g','')
                if (contentLi.indexOf('dimensions') !== -1 || contentLi.indexOf('dimensions du produit') !== -1 || contentLi.indexOf('dimensions du colis') !== -1 && (typeof size === 'undefined' || (!size.indexOf('x') != -1))) size = $(li).text().split(':')[1].trim()
                if (contentLi.indexOf('amazon bestsellers rank') !== -1 || contentLi.indexOf('classement des meilleures') !== -1) categories = $(doc).find('li.zg_hrsr_item span.zg_hrsr_ladder a').map((idx, item) => $(item).text()).toArray()
                // console.log(categories)

                categories = [...categories, ...$(doc).find('#wayfinding-breadcrumbs_feature_div ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray()]
            })
            // console.log(size)

            // console.log(weight, size)
        }

        // console.log(size.length)
        // console.log(categories)
        let cats = []
        let dutycategory
        if (!!document.querySelector('#wayfinding-breadcrumbs_feature_div')) {
            cats = [
                $(doc).find('#wayfinding-breadcrumbs_feature_div ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray().join(' > '),
                ...$(doc).find('.zg_hrsr_ladder').map((v, u) => {
                    return $(u).find('a').map((r, v) => $(v).text().trim()).toArray().join(' > ')//.split('dans ').join('')
                }).toArray()
            ].filter(function (el) {
                return el.trim() != null;
            });
            dutycategory = $(doc).find('#wayfinding-breadcrumbs_feature_div ul li span').map((idx, item) => $(item).text().trim()).toArray().join('')

        } else {
            cats = [
                $(doc).find('#wayfinding-breadcrumbs_container ul li:not(.a-breadcrumb-divider) span').map((idx, item) => $(item).text().trim()).toArray().join(' > '),
                // ...$(doc).find('.zg_hrsr_ladder').map((v, u) => {
                //     return $(u).find('a').map((r, v) => $(v).text().trim()).toArray().join(' > ')//.split('dans ').join('')
                // }).toArray()
            ].filter(function (el) {
                return el.trim() != null;
            });
            dutycategory = $(doc).find('#wayfinding-breadcrumbs_container ul li span').map((idx, item) => $(item).text().trim()).toArray().join('')

        }
        // console.log([...new Set(cats.map(t=>t.trim().toLowerCase().replace(/&/g, 'et')))].join('').replace(/›/g, '>'))
        if (document.location.href.indexOf('amazon.fr') != -1) {
            cats = [[...new Set(cats.map(t => t.trim().toLowerCase().replace(/&/g, 'et')))].join('').replace(/›/g, '>')]
        } else {
            cats = [[...new Set(cats.map(t => t.trim().toLowerCase()))].join('').replace(/›/g, '>')]

        }
        // add extra prohibition
        // console.log(cats)
        // console.log(cats.some(t=>t.indexOf('beauty & personal care')!=-1))
        // console.log(cats.some(t=>t.indexOf('grocery')!=-1))
        categories = cats
        // console.log(prohibited)
        if (!prohibited) {
            // console.log("prohib list")
            //prohib based on category array
            // let catprohib = categories.some(el => {
            //     return getProhibitedCategories().includes(el.trim())
            // })
            // prohib based dutycat
            // let catprohib = getProhibitedCategories().includes(cats[0].replace(/›/g, '>'))
            // console.log(dutycategory)
            let catsProhibLong = cats.some(el => {
                // console.log(el)
                return getProhibitedCategories().map(r => r.toLowerCase().replace(/›/g, '>')/*.split('>').join(' > ')*/).includes(el.trim().toLowerCase().replace(/›/g, '>'))
            })
            // console.log(dutycategory)
            let kwProhib = false
            // Jouets radiocommandés  Hélicoptères Avion
            // console.log(dutycategory)
            if (/*dutycategory.toLowerCase().indexOf('beauté et parfum') != -1 || */dutycategory.toLowerCase().indexOf('abonnements et cartes prépayées') != -1 ||dutycategory.toLowerCase().indexOf('avion') != -1 || dutycategory.toLowerCase().indexOf('hélicoptères') != -1 /*|| dutycategory.toLowerCase().indexOf('radiocommandés') != -1 || dutycategory.toLowerCase().indexOf('epicere') != -1 || dutycategory.indexOf('Epicerie') != -1*/) kwProhib = true
            let catsAllow = getAllowedCategories().includes(dutycategory.replace(/›/g, '>'))
            let catsAllowLong = cats.some(el => {
                // console.log(el)
                return getAllowedCategories().map(r => r.replace(/›/g, '>')/*.split('>').join(' > ')*/).includes(el.trim().replace(/›/g, '>'))
            })
            let prodprohib = getProhibitedProducts().some(el => {
                if($(doc).find('#productTitle').text().replace(/\^/g, '').trim().toLowerCase().includes(el.trim().toLowerCase())) console.log(el);
                // if($(doc).find('#productTitle').text().replace(/\^/g, '').trim().toLowerCase().includes(el.trim().toLowerCase())) console.log($(doc).find('#productTitle').text(), el)
                return $(doc).find('#productTitle').text().replace(/\^/g, '').trim().toLowerCase().includes(el.trim().toLowerCase())
            })
            // console.log(kwProhib)
            prohibited = /*catprohib ||*/ prodprohib || catsProhibLong || kwProhib //|| !catsAllow || !catsAllowLong
            // console.log(kwProhib,prohibited, prodprohib,catsProhibLong)

        }
        let dimensions = {}
        let volumicWeight = 500;
        let wCoef = 1 //g
        let sCoef = 1 //cm
        // console.log(weight)
        let measures
        // if (!weight) console.log("no weight here")
        // if (!size) console.log("no size here")
        if (document.location.origin.indexOf('amazon.fr') != -1) {
            if (size && size.indexOf("—") == -1) {
                // console.log("size there")
                // console.log(size)
                if (size.indexOf(';') != -1) {
                    // console.log("weight in size")

                    let sizWeight = size.split(';')[1].trim()
                    // let pureWeigght = sizWeight.indexOf('Kg')!=-1
                    if (sizWeight.indexOf('kg') != -1 || sizWeight.indexOf('Kg') != -1 || sizWeight.indexOf('Kilogrammes') != -1 || sizWeight.indexOf('kilogrammes') != -1 || sizWeight.indexOf('kg') != -1) wCoef = 1000
                    let applicableWeight = toFloat(sizWeight.replace(' grammes', '').replace(' kilogrammes', '').replace(' Kilogrammes', '').replace(' g', '').replace(' Kg', '').replace(' kg', '').replace(',', '.')) * wCoef
                    measures = size.split(';')[0].trim()
                    let dim = getDimensions(measures.split('x'))
                    // if (measures.indexOf('mm') != -1) sCoef = 10
                    dimensions.length = dim[0]//measures.split('x')[0].trim()
                    dimensions.width = dim[1]//measures.split('x')[1].trim()
                    dimensions.height = dim[2]
                    // console.log(dimensions)
                    // console.log(shipping)
                    // if (measures.indexOf('mm') != -1) sCoef = 10
                    // dimensions.length = measures.split('x')[0].trim()
                    // dimensions.width = measures.split('x')[1].trim()
                    // dimensions.height = measures.split('x')[2].replace(' cm', '').replace(' mm', '').replace(' centimètres', '').trim()
                    // volumicWeight = toFloat(dimensions.length) * sCoef * toFloat(dimensions.height) * sCoef * toFloat(dimensions.width) * sCoef / 5000
                    volumicWeight = dimensions.length * dimensions.height * dimensions.width / 5
                    // volumicWeight = Math.max(volumicWeight, applicableWeight)
                    weight = applicableWeight
                    // console.log(dim)
                    // console.log(weight, volumicWeight)

                    // console.log(weight, size)

                    // console.log(volumicWeight, applicableWeight)
                } else {
                    // console.log("weight not i!n size there")

                    measures = size.trim()
                    let dim2 = getDimensions(measures.split('x'))
                    // console.log(dim2)
                    // console.log(measures)

                    // if (measures.indexOf('mm') != -1) sCoef = 10
                    dimensions.length = dim2[0]//measures.split('x')[0].trim()
                    dimensions.width = dim2[1]//measures.split('x')[1].trim()
                    dimensions.height = dim2[2]//measures.split('x')[2].replace('cm', '').replace('mm', '').trim()
                    // console.log(dimensions)
                    volumicWeight = dimensions.length * dimensions.height * dimensions.width / 5
                    // console.log(weight, volumicWeight)
                    // volumicWeight = Math.max(volumicWeight, 500)
                    if (weight && !weight.includes("—")) {
                        // console.log(weight)
                        if (weight.indexOf('Kg') != -1 || weight.indexOf('Kilogrammes') != -1 || weight.indexOf('kilogrammes') != -1 || weight.indexOf('kg') != -1) wCoef = 1000;
                        weight = toFloat(weight.replace(' grammes', '').replace(' kilogrammes', '').replace(' Kilogrammes', '').replace(' g', '').replace(' Kg', '').replace(' kg', '')) * wCoef
                        // volumicWeight = Math.max(weight, volumicWeight, 500)
                    }

                }

            } else {
                // console.log("no size", weight)

                if (weight && !weight.includes("—")) {
                    // console.log("no size but weight")
                    if (weight.indexOf('Kg') != -1 || weight.indexOf('Kilogrammes') != -1 || weight.indexOf('kilogrammes') != -1 || weight.indexOf('kg') != -1) wCoef = 1000;
                    weight = toFloat(weight.replace(' grammes', '').replace(' kilogrammes', '').replace(' Kilogrammes', '').replace(' g', '').replace(' Kg', '').replace(' kg', '').replace(',', '.')) * wCoef
                    // volumicWeight = Math.max(weight, 500)
                    // volumicWeight = Math.max(toFloat(weight.replace(' grammes', '').replace(' kilogrammes', '').replace(' Kilogrammes', '').replace(' g', '').replace(' Kg', '')) * wCoef, volumicWeight)
                } else {
                    weight = ""
                }
                // if(weight.includes("—"))weight = ""

            }
        }
        if (prodTitle.toLowerCase().indexOf("gonflable") != -1) {
            if (typeof weight === 'undefined' || weight === "") {
                volumicWeight = 500
            } else {
                // if (weight.indexOf('Kg') != -1 || weight.indexOf('Kilogrammes') != -1 || weight.indexOf('kilogrammes') != -1 || weight.indexOf('kg') != -1) wCoef = 1000;
                // weight = toFloat(weight.replace(' grammes', '').replace(' kilogrammes', '').replace(' Kilogrammes', '').replace(' g', '').replace(' Kg', '').replace(' kg', '').replace(',', '.')) * wCoef
                volumicWeight = parseFloat(weight)
            }
        }
        // console.log(shipping)
       
        if($(doc).find("#mir-layout-DELIVERY_BLOCK-slot-DELIVERY_MESSAGE")&&!shipping){
            shipping = $(doc).find("#mir-layout-DELIVERY_BLOCK-slot-DELIVERY_MESSAGE").text().trim()
            // console.log(shipping)
        }
        
        // console.log(shipping)
        if($(doc).find("#shippingMessageInsideBuyBox_feature_div")&&!shipping){
            shipping = $(doc).find("#shippingMessageInsideBuyBox_feature_div").text().trim()
            // console.log(shipping)
        }
        if($(doc).find("#price-shipping-message")&&!shipping){
            shipping = $(doc).find("#price-shipping-message").text().trim()
            // console.log(shipping)
        }
        if($(doc).find("#usedbuyBox") && $(doc).find("#usedbuyBox").text()&&!shipping){
            console.log($(doc).find("#usedbuyBox").text().trim().split('Livraison')[0])
            if($(doc).find("#usedbuyBox").text().indexOf('shipping')!=-1) shipping = $(doc).find("#usedbuyBox").text().trim().split('shipping')[0].split('$')[1].trim()
            if($(doc).find("#usedbuyBox").text().indexOf('Livraison')!=-1) shipping = $(doc).find("#usedbuyBox").text().trim().split('Livraison')[0].split('€')[0].split('+')[0].trim()
            // console.log(shipping)
        }
        if($(doc).find("#ddmDeliveryMessage")&&!shipping){
            shipping = $(doc).find("#ddmDeliveryMessage").text().trim()
        }
        // console.log(shipping)
        if (shipping && typeof shipping === 'string') {
            // console.log($(doc).find("#price-shipping-message").text())
            if ($(doc).find("#price-shipping-message").text().indexOf("EUR 25") != -1||$(doc).find("#price-shipping-message").text().indexOf("Livraison GRATUITE") != -1) isFreeShippingOk = true
            if (shipping.indexOf("FREE Scheduled Delivery") != -1) {
                isFreeShippingOk = true;
                shipping = 0
            }else  if (shipping.indexOf("FREE Shipping") != -1) {
                shipping = 0
            }else   if (shipping.indexOf("Livraison GRATUITE") != -1 || shipping.indexOf("—") != -1) {
                shipping = 0
            } else if (shipping.indexOf("Livraison gratuite") != -1 && !isBook(categories)) {
                // console.log("Livraison gratuite")
                shipping = 500

            }else if (shipping.indexOf("Livraison à") != -1) {
                // console.log("Livraison gratuite")
                shipping = shipping.split('€')[0].split('à')[1].trim()
                // console.log(shipping)
                shipping = priceFormat(shipping)["price"]

            } else if (isBook(categories)) {
                if (bookLivraison(doc)) {
                    // console.log('here')
                    shipping = 1
                } else {

                    shipping = 500
                }
            } else if (shipping.indexOf("Exclusivement pour les membres Amazon Prime") != -1) {
                if ($('#priceBadging_feature_div span span').length) {
                    shipping = priceFormat($('#priceBadging_feature_div span span').text())["price"]
                } else {
                    shipping = 500
                }
            } else {

                shipping = priceFormat(shipping)["price"]
                // console.log(shipping)
            }

        } else {
            if ($(doc).find("#price-shipping-message").text().indexOf("EUR 25") != -1||$(doc).find("#price-shipping-message").text().indexOf("Livraison GRATUITE") != -1) isFreeShippingOk = true
            if (isBook(categories)) {
                // console.log('book category')
                if (bookLivraison(doc)) {
                    // console.log('here')

                    shipping = 1
                } else {
                    // console.log('book category')
                    if ($(doc).find('.buyboxShippingLabel span').length != 0 && !shipping) {
                        shipping = $(doc).find('.buyboxShippingLabel span').text().trim().split('+')[1].split('€')[0].trim().replace(',', '.');
                        shipping = priceFormat(shipping)["price"];
                    }
                    // shipping = 500
                }
            }
        }
        // console.log(typeof shipping)

        if (typeof size === 'undefined') { size = "" } else if (typeof size != undefined) {
            size = " " + size
            if (size.indexOf("—") != -1 || size.indexOf("<") != -1) { size = "" }
        }
        if (typeof weight === 'undefined') {
            weight = ""
        } else if (typeof weight != undefined) {
            weight = "" + weight
            if (weight.indexOf("—") != -1 || weight.indexOf("<") != -1) { weight = "" }
        }
        let shipside
        try {
            // console.log($(doc).find(".shipping3P").text())
            // console.log(doc.querySelector(".shipping3P").textContent)

            shipside = Helper.toNumber($(doc).find(".shipping3P").text().trim().split('+ EUR ')[1].split(' Livraison')[0])
            shipping = shipside
        } catch (error) {
            // console.log(error)
            shipside = 0
        }
        if (!shipping || typeof shipping === 'undefined') {
            // console.log("undefined shipping")
            try {
                // console.log(doc.querySelector('#priceBadging_feature_div').innerText.trim())
                let sh
                try {
                    sh = Helper.toNumber(doc.querySelector('#priceBadging_feature_div').innerText.trim().split('+ EUR ')[1].split(' Livraison')[0])

                } catch (error) {
                    sh = Helper.toNumber(doc.querySelector('#priceBadging_feature_div').innerText.trim().split('+ ')[1].split('€')[0].trim())
                }

                shipping = sh
            } catch (e) {
                shipping = shipside
            }

        }
        // console.log(shipping)
        // if (isNaN(shipping)) console.log("shipping null")
        if (prodTitle.indexOf('Blink ') != -1 && doc.querySelector('#bylineInfo').innerText.indexOf('Blink Home Security') != -1) {
            dimensions = {}
            size = ""
            volumicWeight = weight = 500

        }
        if ((prodTitle.indexOf('Kindle') != -1 || prodTitle.indexOf('Fire TV Stick') != -1 || prodTitle.indexOf('Echo ') != -1 || prodTitle.indexOf('Fire ') != -1) && doc.querySelector('#bylineInfo').innerText.indexOf('Amazon') != -1) {
            // console.log("Kindle or Fire TV Stick")

            dimensions = {}
            size = ""
            volumicWeight = weight = 500
            shipping = 0
        }
        // console.log(shipping)
        // console.log(isBook(categories))
        if (!isBook(categories) && isPrime(doc)) shipping = 0
        // console.log(shipping)

        // if (isBook(categories)) shipping = 1
        let ogShipping = shipping
        if (typeof size === 'undefined') { size = "" } else if (typeof size != undefined) {
            size = " " + size
            if (size.indexOf("—") != -1 || size.indexOf("<") != -1) { size = "" }
        }
        if (typeof weight === 'undefined') {
            weight = ""
        } else if (typeof weight != undefined) {
            weight = " " + weight
            if (weight.indexOf("—") != -1 || weight.indexOf("<") != -1) { weight = "" }
        }
        let sellerName = $(doc).find('#bylineInfo').text().trim()
        let sellerLink = ($(doc).find('#merchant-info a').toArray()[0] ? $(doc).find('#merchant-info a').toArray()[0].href : "")
        // console.log($(sellerLink).attr('href'), prodTitle)
        let deliverySummary = []
        try {

            deliverySummary = getDate(doc).map(v => getDuration(v))
            if (deliverySummary.length > 1) {
                if (deliverySummary[0]["duration"].trim().toLowerCase().indexOf(deliverySummary[1]["duration"].trim().toLowerCase()) != -1) deliverySummary.pop()
            }

        } catch (error) {

        }
        if (doc.getElementById('feature-bullets') && !prohibited) {
            let features = doc.getElementById('feature-bullets').innerText
            if (features.indexOf('butane') != -1 || features.indexOf('propane') != -1) prohibited = true
        }
        if (document.location.href.indexOf('amazon.com') != -1) {
            // prohibition
            // if (cats.some(t => t.indexOf('beauty & personal care') != -1) || cats.some(t => t.indexOf('grocery') != -1)) prohibited = true;
            let prodprohib = getProhibitedProducts().some(el => {
                // console.log($(doc).find('#productTitle').text(), el)
                return cats.some(t => t.toLowerCase().indexOf(el.toLowerCase()) != -1)
            })
            let prodprohib2 = getProhibitedProducts().some(el => {
                // console.log($(doc).find('#productTitle').text(), el)
                try {

                    // console.log(doc.querySelector('title').innerText)
                    return doc.querySelector('title').innerText.toLowerCase().indexOf(el.toLowerCase()) != -1
                } catch (error) {
                    // console.log(error)
                    return false
                }
            })
            // console.log(cats)
            let prohib3 = getProhibitedCategories().some(el => {
                // console.log($(doc).find('#productTitle').text(), el)
                // console.log(cats)
                return cats.some(t => {
                    if (t.toLowerCase().trim().indexOf(el.toLowerCase().trim()) != -1) console.log(t, el)
                    return t.toLowerCase().trim().indexOf(el.toLowerCase().trim()) != -1
                })
            })
            // console.log(prohibited,prodprohib,prodprohib2,prohib3)
            prohibited = prohibited || prodprohib || prodprohib2 || prohib3
            // console.log(prohibited)
            let USdim = getDimensionsUs(doc)
            if (USdim.hasOwnProperty('size')) {
                dimensions.length = USdim.length
                dimensions.height = USdim.height
                dimensions.width = USdim.width
                size = USdim.size
            }
            if (USdim.hasOwnProperty('weight')) {
                weight = USdim.weight
            }
            volumicWeight = USdim.volumicWeight
            // try {
            //     ogShipping = shipping = Math.round(parseFloat(doc.querySelector('#shippingMessageInsideBuyBox_feature_div').textContent.trim().split('shipping')[0].trim().split('$')[1].trim()) * 100)
            // } catch (e) {
            //     // console.log(e)
            // }
            // console.log(USdim)

        }
        // console.log(shipping)
        let color = (doc.querySelector('.swatchSelect')?doc.querySelector('.swatchSelect').title.trim():"no color");
        if(color.indexOf('Cliquez pour sélectionner')!=-1)color = color.split('sélectionner')[1].trim();
        let variantSize = (doc.querySelector('#native_dropdown_selected_size_name option[selected]')?doc.querySelector('#native_dropdown_selected_size_name option[selected]').innerText.trim():"no size");
        if(color.indexOf('Click to select')!=-1)color = color.split('select')[1].trim();
        return {
            color:color,
            variantSize:variantSize,
            dimensions: dimensions,
            shipping: shipping,

            volumetricWeight: volumicWeight,
            weight: weight,
            // dutycategory: dutycategory,
            categories: categories,
            size: size,
            sellerName: sellerName,
            flashTime: getFlashTime(doc),
            // prohibited: false,
            prohibited: prohibited,
            deleted: false,
            forestTaxe: checkExtraTaxe(doc),
            hasFlash: hasFlash(doc),
            flashPrice: getFlashPrice(doc),
            isFreeShippingOk: isFreeShippingOk,
            ogShipping: ogShipping,
            isPrime: isPrime(doc),
            isBook: isBook(categories, doc),
            isSoldByAmazon: isSoldByAmazon(doc),
            isPanierPlus: isPanierPlus(doc),
            deliveryDate: getDate(doc),
            deliveryDuration: deliverySummary.map(d=>d["duration"]),
            deliveryTime: deliverySummary.map(d=>d["time"]).reduce((acc,curr)=>{
                if(curr.toLowerCase().indexOf('invalid')!=-1){
                    return acc;
                }else{
                    acc.push(curr)
                    return acc;
                }
                
            },[]),
           
            sellerLink: sellerLink

            // shipside:shipside
        }
    }
    const parseAmazonFranceProduct = (doc = document) => {
        let title = $(doc).find('title').text()
        let weight = $(doc).find('tr.size-weight:nth-child(1) td.value').text()
        let size = $(doc).find('tr.size-weight:nth-child(2) td.value').text()
        let categories = $(doc).find('li.zg_hrsr_item span.zg_hrsr_ladder a').map((idx, item) => $(item).text()).toArray()
        try {
            let cat = $(doc).find('#SalesRank td.value').text().split(' (')[0].split('en ')[1].trim()
            if (!categories.includes(cat)) categories.unshift(cat)
        } catch (error) {

        }


        return {
            weight: weight,
            categories: categories,
            size: size
        }
    }
    const parseProduct = (doc = document) => {
        if (document.location.href.indexOf('amazon.fr') != -1) {
            return parseAmazonFranceProduct(doc)

        } else if (document.location.href.indexOf('amazon.co.uk') != -1) {
            return parseAmazonUkProduct(doc)
        }
    }
    const getCart = (doc = window.document, isBusiness = false) => {
        // console.log(isBusiness)
        let mainFrame = $(doc).find('#activeCartViewForm');
        // let mainFrame = $('#activeCartViewForm');
        return mainFrame.find('.sc-list-item').filter((idx, item) => {
            return !$(item).attr("data-removed");
        }).map((idx, item) => {
            let prodLink = location.origin + $(item).find(".sc-product-link").attr('href') || '';
            let asin = $(item).attr("data-asin");
            let quantity = parseInt($(item).attr("data-quantity")) || 0;
            let img = $(item).find('.sc-product-image').attr('src') || ''
            let title = $(item).find('.sc-product-title').text().trim() || ''
            let hasVat = false
            // console.log(!!$(item).find("#ccpFirstCustomMessage"))
            if (isBusiness && !!$(item).find(".ccpFirstCustomMessage") && $(item).find(".ccpFirstCustomMessage").text().indexOf('Try to always buy items that have a downloadable VAT invoice') < 0) {
                // console.log($(item).find(".ccpFirstCustomMessage").text())
                hasVat = true
            }
            let pricePage
            try {
                pricePage = $(item).attr("data-price");

                // console.log($(item).find('.sc-product-price').html().trim())
            } catch (error) {
                pricePage = $(item).find('.sc-product-price').html().trim()

            }
            if (isBusiness) {
                if (hasVat) {
                    // console.log(hasVat)
                    try {


                        pricePage = $(item).find('.sc-price-vat-excluded').html().trim();
                    } catch (error) {
                        // console.log(error)
                        hasVat = false
                        pricePage = $(item).attr("data-price");
                    }
                } else {
                    pricePage = $(item).attr("data-price");
                }
            }
            // pricePage = $(item).find('.sc-product-price').html().trim()

            let offeringId = $(item).attr('data-encoded-offering')
            let sellerId
            try {
                sellerId = $(item).find('.sc-seller a').attr("href").split('seller=')[1].trim()

            } catch (error) {
                sellerId = prodLink.split('smid=')[1].split('&')[0].trim()
            }
            let sellerUrl = ''
            if (sellerId && asin) { sellerUrl = document.location.origin + `/dp/${asin}?m=${sellerId}` } else {
                sellerUrl = prodLink.replace('smid', 'm')
            }
            // let sellerUrl = prodLink.replace('smid', 'm')
            let formattedPr = priceFormat(pricePage)
            // console.log(formattedPr)
            let isUsed = !!item.querySelector('.sc-product-condition')
            return {
                url: prodLink,
                asin: asin,
                quantity: quantity,
                image: img,
                title: title,
                price: formattedPr["price"],
                currency: formattedPr["currency"],
                sellerId: sellerId,
                sellerUrl: sellerUrl,
                priceCart: pricePage,
                isUsed: isUsed,
                hasVat: hasVat,
                isBusiness: isBusiness,
                // sellerName:sellerName,
                // total: formattedPr["price"] * quantity,
                source: document.location.origin.split('//')[1],
                offeringId: offeringId,
                version: chrome.runtime.getManifest().version
            }
        }).toArray()
    }
    const getDate = (doc = window.document) => {
        // console.log(doc)
        if (document.location.href.indexOf('amazon.com') != -1) { return getUsDate(doc) }

        try {


            // if (doc.getElementById('ddmDeliveryMessage')) {
            //     let text = doc.getElementById('ddmDeliveryMessage').innerText.trim()
            if (doc.getElementById('ddmDeliveryMessage')|| doc.getElementById('deliveryMessageMirId')) {
                let text = ((doc.querySelector('#ddmDeliveryMessage b')?doc.querySelector('#ddmDeliveryMessage b').innerText.trim():doc.getElementById('ddmDeliveryMessage')?doc.getElementById('ddmDeliveryMessage').innerText.trim():(doc.getElementById('deliveryMessageMirId')?doc.getElementById('deliveryMessageMirId').innerText.trim():"")))
                                // console.log(text)

                if (text.indexOf('?') != -1) {
                    let date = ""
                    if (text.indexOf("hui") != -1) {
                        //Voulez-vous le faire livrer aujourd'hui, jeudi 7 mars? Commandez-le dans les 1 h et 31 mins et choisissez la Livraison ce soir au cours de votre commande. En savoir plus.
                        date = text.split('?')[0].split("hui")[1].split(',').join('').trim()
                        date = (date.split(' ').length > 1 ? date = date.split(' ').slice(1).join(' ') : date)

                        // console.log(date)
                    } else if (text.indexOf("Voulez-vous le faire livrer") != -1) {
                        let date = text.split('Voulez-vous le faire livrer')[1].trim().split('?')[0]
                        console.log(date)
                        return [date]
                    } else {
                        //"Voulez-vous le faire livrer le mercredi 6 mars? Commandez-le dans les 21 h et 30 mins et choisissez la Livraison Express au cours de votre commande. En savoir plus."
                        date = text.split('?')[0].split('le')[3].trim()
                        // console.log(date) 
                        date = (date.split(' ').length > 1 ? date = date.split(' ').slice(1).join(' ') : date)
                        // console.log(date)
                    }
                    //.split(' ')
                    // console.log(date)
                    return [date]
                } else if (text.indexOf('entre') != -1) {
                    //"Faites-vous livrer entre le 20 et le 29 mars en choisissant la Livraison Rapide lors du passage de commande. En savoir plus."
                    // console.log(text.split('entre'))
                    let date = text.split('entre')[1].trim().split('en choisissant')[0]
                    // console.log(date)
                    let dateArray = date.replace(/le/g, '')/*.split('.').join('')*/.split('et').map(t => t.trim())
                    // console.log(dateArray)

                    return dateArray
                } else if (text.indexOf('Livré :') != -1) {
                    let date = ""
                    date = text.split(':')[1].split("Détails.")[0]
                    // console.log(date)
                    return date.split('-').map(r => r.trim())
                } else if (text.indexOf('Livraison GRATUITE : ') != -1) {
                    let date = ""
                    try {
                        date = doc.querySelector('#ddmDeliveryMessage > b').innerText.trim()

                    } catch (error) {
                        date = text.split(':')[1].trim().split("Détails.")[0]

                    }
                    // console.log(date)
                    return [date] //.split('-').map(r => r.trim())
                } else if (text.indexOf('Livraison à') != -1) {
                    let date = ""
                    try {
                        date = doc.querySelector('#ddmDeliveryMessage > b').innerText.trim()

                    } catch (error) {
                        date = text.split(':')[1].trim().split("Détails.")[0]

                    }
                    // console.log(date)
                    return [date] //.split('-').map(r => r.trim())
                } else return [text]
            } else if(doc.querySelector('#mir-layout-DELIVERY_BLOCK-slot-DELIVERY_MESSAGE b')){
                let date = doc.querySelector('#mir-layout-DELIVERY_BLOCK-slot-DELIVERY_MESSAGE b').innerText.trim();
               if(date.indexOf(',')!=-1) {date = date.split(',')[1]}
                if(date.indexOf('-')!=-1) {
                        date = date.split('-').map(r=>r.trim()); 
                        return date;
                        }
                return  [date]
        } else return []
        } catch (error) {
            return []
        }
    }
    const getUsDate = (doc = window.document) => {
        let startDate, endDate, monthGlobal;
        // console.log(doc.getElementById('delivery-message'))
        try {

            let rawDate = (!doc.querySelector('#delivery-message span') ? doc.querySelector('#delivery-message').innerText.split(":")[1].trim() : doc.querySelector('#delivery-message span').innerText);
            let rawDate1 = (!doc.querySelector('#delivery-message') ? doc.querySelector('#delivery-message').innerText.trim() : doc.querySelector('#delivery-message').textContent);
            // console.log(rawDate1)
            if (rawDate1.indexOf('Get it as soon as') != -1) {
                rawDate1 = rawDate1.split('Get it as soon as')[1].split('if')[0].trim()
                startDate = rawDate1.split('-')[0].trim()
                endDate = rawDate1.split('-')[1].trim()

                // console.log([startDate, endDate])
                let date = [startDate, endDate].map(r => {
                    if (r.toLowerCase().indexOf('when') != -1) {
                        //    console.log(r)
                        r = r.split('when')[0]
                        return r
                    }
                    return r
                })
                date.forEach(v => {
                    if (isNaN(v) && v.trim().indexOf(' ') != -1) {
                        monthGlobal = v.split(' ')[0]
                    }

                })
                date = date.map(v => {
                    if (!isNaN(v)) v = monthGlobal + " " + v
                    return v
                })
                return date
            }
            // console.log(rawDate1, doc.querySelector('title').innerText)
            if (rawDate.indexOf('-') != -1) {
                if (!isNaN(rawDate.split('-').map(r => r.trim())[1])) {
                    startDate = rawDate.split('-')[0].trim()
                    endDate = startDate.split(' ')[0].trim() + ' ' + rawDate.split('-')[1].trim()
                    return [startDate, endDate].map(r => {
                        if (r.toLowerCase().indexOf('when') != -1) {
                            r = r.split('when')[0]
                            return r
                        }
                        return r
                    })
                } else {
                    startDate = rawDate.split('-')[0].trim()
                    endDate = rawDate.split('-')[1].trim()
                    return [startDate, endDate].map(r => {
                        if (r.toLowerCase().indexOf('when') != -1) {
                            r = r.split('when')[0]
                            return r
                        }
                        return r
                    })
                }
            } else if (rawDate.indexOf(',') != -1) {
                // console.log("here")
                let date = rawDate.split(',')[1].trim()
                return [date].map(r => {
                    if (r.toLowerCase().indexOf('when') != -1) {
                        r = r.split('when')[0]
                        return r
                    }
                    return r
                })
            } else if (rawDate1.indexOf(',') != -1) {
                // console.log("here")
                let date = rawDate1.split(',')[1].trim()
                return [date].map(r => {
                    if (r.toLowerCase().indexOf('when') != -1) {
                        r = r.split('when')[0]
                        return r
                    }
                    return r
                })
            }
            else if (document.querySelector('#delivery-message span:nth-child(2)') && document.querySelector('#delivery-message span:nth-child(1)').indexOf('') != -1) {
                // console.log('what')
            }
        } catch (error) {
            
            if(doc.querySelector('#mir-layout-DELIVERY_BLOCK-slot-DELIVERY_MESSAGE b')){
                let date = doc.querySelector('#mir-layout-DELIVERY_BLOCK-slot-DELIVERY_MESSAGE b').innerText.trim();
               if(date.indexOf(',')!=-1) {date = date.split(',')[1]}
                if(date.indexOf('-')!=-1) {
                        date = date.split('-').map(r=>r.trim()); 
                        return date;
                        }
                return  [date]
        }
            // console.log(error)
            return []
        }
    }
    const getDuration = (date) => {
        // console.log(date)
        let lang = 'en'
        let format = 'MMMM DD'
        if (document.location.href.indexOf('amazon.fr') != -1) {
            lang = 'fr'
            format = 'DD MMM'
        }
        const desiredWeekday = 4;
        // Thursday
        if (date.indexOf(',') != -1) {
            format = 'MMM DD'
            date = date.map(d => {
                if (r.indexOf(',') != -1) return r.split(',')[1]
                return r
            })
        }
        if (date.indexOf('demain') != -1) {
            date = moment().add(1, 'days');
        }
        const currentWeekday = moment().isoWeekday();
        const dateDay = moment(date, format, lang).isoWeekday();

        const missingDays = ((desiredWeekday - dateDay) + 7) % 7;
        // console.log(missingDays,missingDays+2*7)
        const nextThursday = moment(date, format, lang).add({ days: missingDays, weeks: 2 })
        if(nextThursday.isBefore()) nextThursday.add(1, 'years')
        // console.log(nextThursday._d,nextThursday.isBefore(),nextThursday.add(1, 'years'))
        if (document.location.href.indexOf('amazon.fr') != -1) {
            return {duration:nextThursday.format('LL'),time:nextThursday.format('L').split('/').reverse().join('-')}

        }
        if (document.location.href.indexOf('amazon.com') != -1) {
            return {
                duration:nextThursday.format('LL'),
                time:nextThursday.format('L').split('/').reverse().reduce((acc,curr,idx,arr)=>{
                    if(idx == 0) acc.push(curr)   

                    if(idx == 1) acc.push(arr[2])   
                    if(idx == 2) acc.push(arr[1])   
                    return acc;
                    },[]).join('-')
                }

        }
    }
    // console.log(getDuration(getDate(document)))
    // console.log(getUsDate())

    return {
        getCart: getCart,
        parseProduct: parseAmazonProduct,
        isCartPage: isCartPage,
        isProductPage: isProductPage,
        goToCart: goToCart,
        isAddedItemPage: isAddedItemPage,
        storageKey: getAmazonKey(),
        getProhibitedCategories: getProhibitedCategories,
        getDomain: getDomain,
        getDate: getDate,
        getDuration: getDuration,
        getVatNumber: getVatNumber
    }
})()
var CAPModule = amazon;
// alert('injected')