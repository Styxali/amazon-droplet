const csv = require('csv-parser');
const fs = require('fs');

let tree = {}
fs.createReadStream('all.csv')
  .pipe(csv())
  .on('data', (row) => {
    // console.log(row);
    if(!tree.hasOwnProperty(row["unspsc_code"])) {
      tree[row["unspsc_code"]]=[];
      tree[row["unspsc_code"]].push({category:row["category_desc"],subcategory:row["subcategory_desc"]})
    } else  
    if(!tree[row["unspsc_code"]].some(el=>el["category"]==row["category_desc"]&&el["subcategory"]==row["subcategory_desc"])){
        tree[row["unspsc_code"]].push({category:row["category_desc"],subcategory:row["subcategory_desc"]})
      }
 

  })
  .on('end', () => {
    fs.writeFile('tree.json', JSON.stringify(tree), 'utf8', ()=> {console.log("done")}); // write it back }});
    console.log('CSV file successfully processed');
  });