// var zlib = require('zlib');
var request = require('request-promise');
var cheerio = require('cheerio');
var URL = require('url').URL;
const j = request.jar();
// var parser = require('./amazonHeleper')
var helper = require('./helper')

const reqCookie = request.defaults({ jar: j, gzip: true/*,encoding: null, proxy:"https://69.65.65.178:59720"*/ })
const getShipping = () => {
    // return reqCookie()
}
const getOfferListing = async (asin) => {
    let url = `https://www.amazon.com/gp/offer-listing/${asin}`
    let data = await reqCookie(url);
    let $ = cheerio.load(data)
    let offer = $('.olpOffer')
        .toArray()
        .map(el => {
            let offeringId = $(el).find('input[name="offeringID.1"]').attr('value')
            let price = $(el).find('.olpOfferPrice').text().trim()
            let seller = $(el).find('.olpSellerName').text().trim()
            let sellerUrl = $(el).find('.olpSellerName a').attr('href')
            let shipping = ($(el).find('.olpShippingInfo').text().indexOf('FREE Shipping') != -1 ? 0 : $(el).find('.olpShippingPrice').text().trim())
            let name = $(el).find('.olpSubHeadingSection').text().trim()
            let url = new URL("https://www.amazon.com" + sellerUrl);
            let params = new URLSearchParams(url.search);
            let isFBA = params.get("isAmazonFulfilled")
            let sellerId = params.get("seller")
            let isPrime = (!!$(el).find('.supersaver') ? true : false)
            let isUsed = ($(el).find('.olpCondition').text().indexOf('New') != -1 ? false : true)
            return { offeringId, isPrime, price, isUsed, seller, sellerUrl, shipping, isFBA, sellerId }
        })
    // console.log($("#glow-ingress-block").text(), offer)
    return offer
}
const getOfferListingFr = async (asin) => {
    let url = `https://www.amazon.fr/gp/offer-listing/${asin}`
    let data = await reqCookie(url);
    let $ = cheerio.load(data)
    let offer = $('.olpOffer')
        .toArray()
        .map(el => {
            let offeringId = $(el).find('input[name="offeringID.1"]').attr('value')
            let price = $(el).find('.olpOfferPrice').text().trim()
            let seller = $(el).find('.olpSellerName').text().trim()
            let sellerUrl = $(el).find('.olpSellerName a').attr('href')
            let shippingRaw = ($(el).find('.olpShippingInfo').text().indexOf('FREE Shipping') != -1 ? 0 : $(el).find('.olpShippingPrice').text().trim())
            let shipping= (typeof shippingRaw ==="string"?parseFloat(shippingRaw.split('EUR ')[1].replace(',','.'))*100:0)
            let name = $(el).find('.olpSubHeadingSection').text().trim()
            let url = new URL("https://www.amazon.fr" + sellerUrl);
            let params = new URLSearchParams(url.search);
            let isFBA = params.get("isAmazonFulfilled")
            let sellerId = params.get("seller")
            let isPrime = (!!$(el).find('.supersaver').length ? true : false)
            let isUsed = ($(el).find('.olpCondition').text().indexOf('New') != -1 ? false : true)
            
            return {shippingRaw, offeringId, isPrime, price, isUsed, seller, sellerUrl, shipping, isFBA, sellerId }
        })
    // console.log($("#glow-ingress-block").text(), offer)
    return offer
}
const getProductFr = async (asin) => {
    try {

    let url = `https://www.amazon.fr/dp/${asin}`
    let data = await reqCookie(url);
    let $ = cheerio.load(data)
    let dimensions = helper.getDimensions(data)
    let flashPrice = helper.getFlashPrice(data)
    let flashTime ="0"
    

     flashTime = helper.getFlashTime(data)


    let hasFlash = helper.hasFlash(data)
    let isFreeShippingOk = helper.isFreeShippingOk(data)
    let isPanierPlus = helper.isPanierPlus(data)
    let isPrime = helper.isPrime(data)
    let isSoldByAmazon = helper.isSoldByAmazon(data)
    let metadata = helper.getAllData(data)
    let forestTaxe = helper.hasForestTaxe(data)
    let categories = [$('#wayfinding-breadcrumbs_feature_div ul li').toArray().map(el => $(el).text().trim()).join('>')]
    flashTime = (!!flashTime?flashTime:"0")

    let isBook = helper.isBook(categories,data)
       let sellerLink = helper.getSellerLink(data)
    console.log($("#glow-ingress-block").text(), flashTime)
    
    let size = helper.getSize(data)
    let weight = helper.getWeight(data)
    // console.log(dataSize,dataWeight)
    
    isFound = true
    return { categories, isFound,size,weight, dimensions,isBook,sellerLink, flashPrice, flashTime, hasFlash, isFreeShippingOk, isSoldByAmazon, isPanierPlus, isPrime,forestTaxe, metadata };
} catch (error) {
    console.log(error)
    return {isFound}
}
}
const getProduct = async (asin) => {
    let url = `https://www.amazon.com/dp/${asin}`
    let data = await reqCookie(url);
    let $ = cheerio.load(data)
    // let data = parser.parseProduct(data)
    let dimensions = helper.getDimensions(data)
    let flashPrice = helper.getFlashPrice(data)

    let flashTime = helper.getFlashTime(data)
    let hasFlash = helper.hasFlash(data)
    let isFreeShippingOk = helper.isFreeShippingOk(data)
    let isPanierPlus = helper.isPanierPlus(data)
    let isPrime = helper.isPrime(data)
    let isSoldByAmazon = helper.isSoldByAmazon(data)
    let metadata = helper.getAllData(data)


    // let categories = $('#wayfinding-breadcrumbs_feature_div ul li').toArray().map(el => $(el).text().trim()).join('>')
    // console.log($("#glow-ingress-block").text(), categories)
    isFound = true
    let categories = $('#wayfinding-breadcrumbs_feature_div ul li').toArray().map(el => $(el).text().trim()).join('>')

    return { categories, metadata, dimensions, isFound, flashPrice, flashTime, hasFlash, isFreeShippingOk, isSoldByAmazon, isPanierPlus, isPrime };
}
const getUsCartUs = async (cart) => {
    console.log("us cart")
    try {

        await reqCookie("https://www.amazon.com/", { "credentials": "include", "headers": { "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3", "accept-language": "en-US,en;q=0.9,fr;q=0.8,de-AT;q=0.7,de;q=0.6", "cache-control": "no-cache", "pragma": "no-cache", "sec-fetch-mode": "navigate", "sec-fetch-site": "same-origin", "sec-fetch-user": "?1", "upgrade-insecure-requests": "1" }, "referrer": "https://www.amazon.com/", "referrerPolicy": "no-referrer-when-downgrade", "body": null, "method": "GET", "mode": "cors" })
        await reqCookie("https://www.amazon.com/gp/delivery/ajax/address-change.html", {
            // "credentials": "include", 
            "headers": {
                "accept": "text/html,*/*",
                "accept-language": "en-US,en;q=0.9,fr;q=0.8,de-AT;q=0.7,de;q=0.6", "cache-control": "no-cache",
                "content-type": "application/x-www-form-urlencoded;charset=UTF-8", "pragma": "no-cache", "sec-fetch-mode": "cors", "sec-fetch-site": "same-origin", "x-requested-with": "XMLHttpRequest"
            }, "referrer": "https://www.amazon.com/", "referrerPolicy": "no-referrer-when-downgrade", "body": "locationType=LOCATION_INPUT&zipCode=19706&storeContext=generic&deviceType=web&pageType=Gateway&actionSource=glow",
            "method": "POST",
            // "mode": "cors"
        })


    } catch (error) {
        console.log(error)
    }
    // console.log(resp)
    let resp = await reqCookie("https://www.amazon.com/", {
        // "credentials":"include",
        "headers": {
            "accept": "text/html,application/xhtml+xml",
            "accept-language": "en-US,en;q=0.9,fr;q=0.8,de-AT;q=0.7,de;q=0.6",
            "cache-control": "no-cache",
            "pragma": "no-cache",
            // "sec-fetch-mode":"navigate","sec-fetch-site":"same-origin","sec-fetch-user":"?1",
            "upgrade-insecure-requests": "1"
        }, "referrer": "https://www.amazon.com/",
        "referrerPolicy": "no-referrer-when-downgrade", "body": null, "method": "GET",
        // "mode":"cors"
    })
    var $ = cheerio.load(resp)
    // console.log(resp)

    if ($("#glow-ingress-block").text().indexOf('19706') != -1) {
        return await Promise.all(cart.map(async item => {
         
            try {
                let asin = item["asin"]
                let product 
                try {

                    product = await getProduct(asin)
                } catch (error) {
                    return item
                }
                try {
                    let offers = await getOfferListing(asin)
                product.offerlisting = offers

                }catch(e){
                    console.log("no offerListing")
                }

                return { ...product, ...item }
            } catch (e) {
                console.log(e)
                let isFound = false
                return { isFound }
            }
        }))
        // console.log(data/*.map(d => d.map(v => v["shipping"]))*/)
    }
}
const getFrCart = async (cart) => {
    try {
        await reqCookie("https://www.amazon.fr/", { "credentials": "include", "headers": { "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3", "accept-language": "en-US,en;q=0.9,fr;q=0.8,de-AT;q=0.7,de;q=0.6", "cache-control": "no-cache", "pragma": "no-cache", "sec-fetch-mode": "navigate", "sec-fetch-site": "same-origin", "sec-fetch-user": "?1", "upgrade-insecure-requests": "1" }, "referrer": "https://www.amazon.fr/", "referrerPolicy": "no-referrer-when-downgrade", "body": null, "method": "GET", "mode": "cors" })
        await reqCookie("https://www.amazon.fr/gp/delivery/ajax/address-change.html", { "credentials": "include", "headers": { "accept": "text/html,*/*", "accept-language": "en-US,en;q=0.9,fr;q=0.8,de-AT;q=0.7,de;q=0.6", "cache-control": "no-cache", "content-type": "application/x-www-form-urlencoded;charset=UTF-8", "pragma": "no-cache", "sec-fetch-mode": "cors", "sec-fetch-site": "same-origin", "x-requested-with": "XMLHttpRequest" }, "referrer": "https://www.amazon.fr/gp/offer-listing/2223006965/", "referrerPolicy": "no-referrer-when-downgrade", "body": "locationType=LOCATION_INPUT&zipCode=95500&storeContext=generic&deviceType=web&pageType=OfferListing&actionSource=glow", "method": "POST", "mode": "cors" })
        let resp = await reqCookie("https://www.amazon.fr/", { "credentials": "include", "headers": { "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3", "accept-language": "en-US,en;q=0.9,fr;q=0.8,de-AT;q=0.7,de;q=0.6", "cache-control": "no-cache", "pragma": "no-cache", "sec-fetch-mode": "navigate", "sec-fetch-site": "same-origin", "sec-fetch-user": "?1", "upgrade-insecure-requests": "1" }, "referrer": "https://www.amazon.fr/", "referrerPolicy": "no-referrer-when-downgrade", "body": null, "method": "GET", "mode": "cors" })
        // console.log(resp)
        var $ = cheerio.load(resp)
        // console.log($("#glow-ingress-block").text())
        if ($("#glow-ingress-block").text().indexOf('95500') != -1) {
            return await Promise.all(cart.map(async item => {
                try {
                    let asin = item["asin"]
                    let product 
                    try {
    
                        product = await getProductFr(asin)
                    } catch (error) {
                        return item
                    }
                    try {
                        let offers = await getOfferListingFr(asin)
                    product.offerlisting = offers
    
                    }catch(e){
                        console.log("no offerListing")
                    }
                    
                    product =  { ...product, ...item }
                    try {
                        
                    
                    let currentOffer = product.offerlisting.filter(r=>r["sellerId"]==product["sellerId"])[0]
                    product.ogShipping = currentOffer["shipping"]
                    product.shipping = currentOffer["shipping"]
                    product.total = (product.shipping+product.price)*product["quantity"]
                } catch (error) {
                        console.log(error)
                }
                    return product 
                } catch (e) {
                    console.log(e)
                    let isFound = false
                    return { isFound }
                }
            }))
            // console.log(data/*.map(d => d.map(v => v["shipping"]))*/)
        }
    } catch (e) {
        console.log(e)
        // let isFound = false
        return { e }
    }
}
module.exports = {
    getUsCartUs,
    getFrCart
}
